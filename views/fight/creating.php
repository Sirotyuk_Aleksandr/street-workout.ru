<?php
$this->title = 'Создание соревнования';

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\date\DatePicker;

?>
<div class="container-fluid">
    <?php
    $form = ActiveForm::begin(['class' => 'form-horizontal', 'id' => 'registration_form']);
    ?>
    <?= $form->field($creatingForm, 'city')
        ->textInput(['placeholder' => 'Введите название города, например г.Москва']); ?>
    <?= $form->field($creatingForm, 'playground_address')
        ->textInput(['placeholder' => 'Введите адрес площадки, например ул.Ленина 24 или парк Пушкина']); ?>
    <?= $form->field($creatingForm, 'date_fight')->widget(DatePicker::className(), [
        'type' => DatePicker::TYPE_INPUT,
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd'
        ]
    ]); ?>
    <?= $form->field($creatingForm, 'id_exercise1')->dropDownList($exercises,
        [
            'prompt' => 'Выберите первое упражнение'
        ]); ?>
    <?= $form->field($creatingForm, 'id_exercise2')->dropDownList($exercises,
        [
            'prompt' => 'Выберите второе упражнение'
        ]); ?>
    <?= $form->field($creatingForm, 'id_exercise3')->dropDownList($exercises,
        [
            'prompt' => 'Выберите третье упражнение'
        ]); ?>
    <div>
        <button type="submit" id="fight_offer" class="btn btn-primary">Вызвать на зарубу</button>
    </div>
    <?php
    ActiveForm::end();
    ?>
</div>
