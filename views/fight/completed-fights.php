<?php

$this->title = 'Зарубы';
use yii\helpers\Url;
use yii\widgets\Pjax;
?>
<?php Pjax::begin([
    'timeout' => 5000,
    'clientOptions' => ['method' => 'POST'],
    'enablePushState' => false,
]); ?>
    <section class="fights">
        <div class="container-fluid">
            <h3 align="center"><a href="<?= Url::to(['/fights/upcoming-fights']) ?>">Список ближайших заруб</a> | Недавно завершенные зарубы</h3>
            <div class="fight-block">
                <table width="100%" cellpadding="5" border="1">
                    <tr>
                        <th>Номер зарубы</th>
                        <th>Город</th>
                        <th>Адрес площадки</th>
                        <th>Дата проведения</th>
                        <th>Упражнения</th>
                        <th>Первый участик</th>
                        <th>Второй участник</th>
                    </tr>
                    <?php
                    if(!empty($fights))
                        foreach ($fights as $fight):
                            if ($fight['date_fight'] < date('Y-m-d')):
                                ?>
                                <tr>
                                    <td><?=$fight['id'];?></td>
                                    <td><?=$fight['city'];?></td>
                                    <td><?=$fight['playground_address'];?></td>
                                    <td><?=(new DateTime($fight['date_fight']))->format('d.m.Y'); ?></td>
                                    <td>
                                        <?php
                                            foreach ($exercises as $exercise)
                                                if($fight['id'] == $exercise['id_fight'])
                                                    echo $exercise['name_exercise']."<br/>";
                                        ?>
                                    </td>
                                    <?php foreach ($participants as $participant):
                                        if($fight['id'] == $participant['id_fight']):
                                            ?>
                                            <td><a href="<?= Url::to(['/site/view-user', 'id' => $participant['id']]) ?>">
                                                    <?=$participant['login'];?></a></td>
                                        <?php
                                        endif;
                                    endforeach;
                                    ?>
                                </tr>
                            <?php
                            endif;
                        endforeach;
                    ?>
                </table>
            </div>
        </div>
    </section>
<?php Pjax::end();?>