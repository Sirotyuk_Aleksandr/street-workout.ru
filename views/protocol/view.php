<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UserCompetition */

$this->title = $model->id_user;
$this->params['breadcrumbs'][] = ['label' => 'Объявление результата соревнований', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-competition-view">

    <h1>Объявление результатов соревнования</h1>

    <p>
        <?= Html::a('Update', ['update', 'id_user' => $model->id_user, 'id_competition' => $model->id_competition],
            ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id_user' => $model->id_user, 'id_competition' => $model->id_competition],
            [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_user',
            'id_competition',
            'place',
            'result',
        ],
    ]) ?>

</div>
