<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserCompetition */

$this->title = 'Update User Competition: ' . $model->id_user;
$this->params['breadcrumbs'][] = ['label' => 'User Competitions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_user, 'url' => ['view', 'id_user' => $model->id_user, 'id_competition' => $model->id_competition]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-competition-update">
    <div class="container-fluid">
        <div class="col-md-5 col-md-offset-4">
            <h1>Объявление результата</h1>

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
