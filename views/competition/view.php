<?php
$this->title = $competition[0]['competition_name'];

use yii\helpers\Url;
use yii\widgets\Pjax;
?>
<?php Pjax::begin([
    'timeout' => 5000,
    'clientOptions' => ['method' => 'POST'],
    'enablePushState' => true,
]); ?>
<div class="container-fluid">
    <h3 align="center">Сведения о соревновании |<a href="<?= Url::to(['/competition/view-participants',
            'id' => $competition[0]['id_competition']]) ?>"> Участники соревнования</a>
        <?php if($competition[0]['id_creator'] == Yii::$app->user->identity['id']):?>
            | <a href="<?= Url::to(['/protocol/index', 'id' => $competition[0]['id_competition']]) ?>">
                Объявить результат
            </a>
        <?php endif;?>
    </h3>
    <div class="col-sm-4 col-sm-offset-4">
        <div class="competition_view">
            <h2><?= $competition[0]['competition_name']?></h2>
            <ul>
                <li><h4>Город:</h4> <p><?= $competition[0]['city']?></p></li>
                <li>
                    <h4>Логин создателя соревнования:</h4>
                    <p>
                        <?= $competition[0]['last_name']?>
                        <?= $competition[0]['name']?>
                        <?= $competition[0]['patronymic']?>
                        <a href="<?= Url::to(['/site/view-user', 'id' => $competition[0]['id_creator']]) ?>">
                            <?= '( '.$competition[0]['login'].' )'?></a>
                    </p>
                </li>
                <li><h4>Тип соревнования:</h4> <p><?= $competition[0]['type_name']?></p></li>
                <li><h4>Дата проведения:</h4> <p><?= $competition[0]['date']?></p></li>
                <li><h4>Необходимый взнос:</h4> <p><?= $competition[0]['contribution']?></p></li>
                <li><h4>Возрастные ограничения:</h4> <p>От <?= $competition[0]['age_from']?> До <?= $competition[0]['age_to']?> лет</p></li>
                <li><h4>Описание соревнования:</h4> <p><?= $competition[0]['description']?></p></li>
                <li><h4>Типы наград:</h4>
                    <p>
                        <?php
                            foreach ($awards as $award)
                                echo $award['name'].', ';
                        ?>
                    </p>
                </li>
                <li><h4>Спонсоры:</h4>
                <p>
                   <?php
                        foreach ($sponsors as $sponsor)
                            echo $sponsor['name'].', ';
                    ?>
                </p>
                </li>
                <?php  if ($competition[0]['date'] >= date('Y-m-d')): ?>
                <a data-id_competition="<?=$competition[0]['id_competition']?>" href="<?= Url::to(['/competition/add-participant', 'id_competition' => $competition[0]['id_competition']]) ?>"
                   class="btn btn-success add_participant">Принять участие</a><br/>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</div>
<?php Pjax::end();?>

<!--    <div class="col-md-3 col-md-offset-4">-->
<!--        <div class="competition_participants">-->
<!--            --><?php
//            foreach ($participants as $participant):
//                echo $participant['last_name']. ' '. $participant['name']. ' '. $participant['patronymic'].
//                    ' (';
//                ?>
<!--                <a href="--><?//= Url::to(['/site/view-user', 'id' => $participant['id']]) ?><!--">--><?//=$participant['login']?><!--</a>-->
<!--                --><?//= ')'; ?><!--<br/>-->
<!--            --><?php //endforeach; ?>
<!--        </div>-->
<!--    </div>-->

<!--<div class="slide">-->
<!--    <label for="slide1">-->
<!--        Показать участников соревнования-->
<!--    </label>-->
<!--    <input type="checkbox" id="slide1"/>-->
<!--    <p class="content">-->
<!--        --><?php
//        foreach ($participants as $participant):
//            echo $participant['last_name']. ' '. $participant['name']. ' '. $participant['patronymic'].
//                ' (';
//            ?>
<!--            <a href="--><?//= Url::to(['/site/view-user', 'id' => $participant['id']]) ?><!--">--><?//=$participant['login']?><!--</a>-->
<!--            --><?//= ')'; ?><!--<br/>-->
<!--        --><?php //endforeach; ?>
<!--    </p>-->
<!--</div>-->

