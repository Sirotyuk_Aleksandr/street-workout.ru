<?php
$this->title = $competition[0]['competition_name'];

use yii\helpers\Url;
use yii\widgets\Pjax;
?>
<?php Pjax::begin([
    'timeout' => 5000,
    'clientOptions' => ['method' => 'POST'],
    'enablePushState' => true,
]); ?>
<div class="container-fluid">
    <h3 align="center"><a href="<?= Url::to(['/competition/view', 'id' => $competition[0]['id_competition']]) ?>">
        Сведения о соревновании</a> | Участники соревнования
        <?php if($competition[0]['id_creator'] == Yii::$app->user->identity['id']):?>
         | <a href="<?= Url::to(['/protocol/index', 'id' => $competition[0]['id_competition']]) ?>">
                Объявить результат
            </a>
        <?php endif;?>
    </h3>
        <div class="view_participants">
            <h2><?= $competition[0]['competition_name']?></h2>
            <table width="60%" cellpadding="5">
                <tr>
                    <th>ФИО</th>
                    <th>Логин</th>
                    <th>Результат</th>
                    <th>Место</th>
                </tr>
                 <?php
                    foreach ($participants as $participant):
                 ?>
                <tr>
                    <td><?=$participant['last_name']. ' '. $participant['name']. ' '. $participant['patronymic'];?></td>
                    <td><a href="<?= Url::to(['/site/view-user', 'id' => $participant['id']]) ?>"><?=$participant['login']?></a></td>
                    <?php if($participant['result'] != null):?>
                        <td><?= $participant['result']?></td>
                    <?php endif;?>
                    <?php if($participant['place'] != null):?>
                        <td><?= $participant['place']?></td>
                    <?php endif;?>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
</div>
<?php Pjax::end();?>


