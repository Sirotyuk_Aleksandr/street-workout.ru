<?php
$this->title = 'Создание соревнования';

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
?>
<?php Pjax::begin([
    'timeout' => 5000,
    'clientOptions' => ['method' => 'POST'],
    'enablePushState' => false,
]); ?>
<div class="container-fluid">
    <h3 align="center"><a href="<?= Url::to(['/competitions']) ?>">Список соревнований</a> | Создать соревнование
        | <a href="<?= Url::to(['/competitions/completed']) ?>">Завершенные соревнования</a></h3>
    <?php if(!$flagProhibition): ?>
        <div class="col-md-5 col-md-offset-4 form-creating">
            <?php
            $form = ActiveForm::begin(['class' => 'form-horizontal', 'id' => 'registration-form']);
            ?>
            <?= $form->field($creatingForm, 'competition_name'); ?>
            <?= $form->field($creatingForm, 'city'); ?>
            <?= $form->field($creatingForm, 'id_type_competition')->dropDownList($types_competitions,
                [
                    'prompt' => 'Выберите тип соревнования'
                ]); ?>
            <?= $form->field($creatingForm, 'date')->widget(DatePicker::className(), [
                'type' => DatePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]); ?>
            <?= $form->field($creatingForm, 'age_from')
                ->textInput(['placeholder' => 'Введите одно число, например 14']); ?>
            <?= $form->field($creatingForm, 'age_to')
                ->textInput(['placeholder' => 'Введите одно число, например 18']); ?>
            <div>
                <button type="submit" class="btn btn-primary">Создать соревнование</button>
            </div>
            <?php
            ActiveForm::end();
            ?>
        </div>
    <?php else: ?>
        <p align="center" class="log_in">Вы не можете создавать соревнования.</p>
    <?php endif; ?>
</div>
<?php Pjax::end();?>