<?php

$this->title = 'Завершенные соревнования';
use yii\helpers\Url;
use yii\widgets\Pjax;
?>
<?php Pjax::begin([
    'timeout' => 5000,
    'clientOptions' => ['method' => 'POST'],
    'enablePushState' => false,
]); ?>
    <section class="competitions">
        <div class="container-fluid">
            <h3 align="center"><a href="<?= Url::to(['/competitions']) ?>">Список соревнований</a>
                | <a href="<?= Url::to(['/competitions/creating']) ?>">Создать соревнование</a> | Завершенные соревнования</h3>
            <div class="competition-block">
                <table width="100%" cellpadding="5" border="1">
                    <tr>
                        <th>Город</th>
                        <th>Название</th>
                        <th>Создатель</th>
                        <th>Тип соревнования</th>
                        <th>Дата проведения</th>
                        <th>Возрастные ограничения</th>
                        <th></th>
                    </tr>
                    <?php
                    if(!empty($competitions))
                        foreach ($competitions as $competition):
                            if ($competition['date'] < date('Y-m-d')):
                    ?>
                            <tr>
                                <td><?=$competition['city'];?></td>
                                <td><?=$competition['competition_name'];?></td>
                                <td><a href="<?= Url::to(['/site/view-user', 'id' => $competition['id_creator']]) ?>"><?=$competition['login'];?></a></td>
                                <td><?=$competition['type_name'];?></td>
                                <td><?=(new DateTime($competition['date']))->format('d.m.Y'); ?></td>
                                <td>От <?=$competition['age_from'];?> до <?=$competition['age_to'];?> лет</td>
                                <td><a data-pjax="0" href="<?= Url::to(['/competition/view', 'id' => $competition['id_competition']]) ?>" class="btn btn-success">Подробнее</a></td>
                            </tr>

                        <?php
                            endif;
                            endforeach;
                        ?>
                </table>
            </div>
        </div>
    </section>
<?php Pjax::end();?>