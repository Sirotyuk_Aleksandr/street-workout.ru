<?php
$this->title = 'Обратная связь';

use yii\widgets\ActiveForm;
?>
<div class="container-fluid">
    <div class="col-md-4 col-md-offset-4">
        <h1 align="center">Обратная связь</h1>

        <?php
        $form = ActiveForm::begin(['class' => 'form-horizontal', 'id' => 'authorization-form']);
        ?>
        <?= $form->field($feedbackForm, 'name'); ?>
        <?= $form->field($feedbackForm, 'email'); ?>
        <?= $form->field($feedbackForm, 'content')->textarea(); ?>
        <div>
            <button type="submit" class="btn btn-primary">Отправить</button>
        </div>
        <?php
        ActiveForm::end();
        ?>
    </div>
</div>