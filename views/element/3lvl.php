<?php
$this->title = 'Элементы воркаута (1 уровень)';

use yii\helpers\Html;
use yii\helpers\Url;
?>

<section class="elements_lvl">
    <div class="container-fluid">
        <?php
        if(!empty($element))
            foreach ($element as $element):
        ?>
            <div class="col-md-4">
                <span class="element-block">
                    <h3><?=$element['name']; ?></h3>
                    <?= Html::img("@web/images/elements/3lvl/{$element['image']}", ['alt' => '1 lvl', 'height' => '180px', 'width' => '210px']); ?>
                    <a data-id="<?=$element['id']?>" href="<?= Url::to(['/element/add', 'id' => $element['id']]) ?>" class="btn btn-success add_element">Добавить в изученное</a><br/>
                </span>
            </div>
        <?php endforeach; ?>
    </div>
</section>
