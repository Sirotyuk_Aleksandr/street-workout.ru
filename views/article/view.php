<?php

$this->title = $article[0]['name_article'];
$this->registerMetaTag(['name' => 'keywords', 'content' => $article['name']]);

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>

<section class="article">
    <div class="container-fluid">
        <div class="row">
            <span class="article-block">
                <?php if(!empty($article)): ?>
                    <?= Html::img("@web/images/uploads/articles/{$article[0]['main_image']}",
                        ['alt' => $article[0]['name_article'], 'height' => '240px', 'width' => '290px']); ?>

                         <div class="article_headers2">
                                <?=$article[0]['name_article'];?>
                             <?php if($article[0]['is_advertising'] == '1'):?>
                                 (на правах рекламы)
                             <?php endif; ?>
                            </div>
                        <h4>Автор:</h4>
                         <a href="<?= Url::to(['site/view-user',
                             'id' => $article[0]['id_author']]) ?>"><?= $article[0]['login']; ?></a>
                        <h4>Дата создания:</h4>
                        <?= $article[0]['date_publication']; ?>
                        <div class="content-article-block">
                            <?= $article[0]['content']; ?>
                        </div>

            </span>
        </div>
    <?php endif; ?>
</section>