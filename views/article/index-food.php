<?php

$this->title = 'Обучающие статьи';

use yii\helpers\Html;
use yii\helpers\Url;
?>

<section class="articles">
    <div class="container-fluid">
        <?php if(!Yii::$app->user->isGuest): ?>
            <h2 align="center"><a href="<?= Url::to(['/article/create',
                    'categoryId' => 4]) ?>">Предложить статью</a></h2>
        <?php endif; ?>
        <?php
        if(!empty($articles))
            foreach ($articles as $article):
                ?>
                <div class="article-block">
                    <div class="row">
                        <hr/>
                        <div class="col-md-3">
                            <?= Html::img("@web/images/uploads/articles/{$article['main_image']}", ['alt' => $article['name'],
                                'height' => '240px', 'width' => '290px']); ?>
                        </div>
                        <div class="col-md-4">
                            <div class="preview">
                                <div class="article_headers2">
                                    <?=$article['name_article'];?>
                                    <?php if($article['is_advertising'] == '1'):?>
                                    (на правах рекламы)
                                    <?php endif; ?>
                                </div>
                                <h4>Автор:</h4>
                                <a href="<?= Url::to(['site/view-user',
                                    'id' => $article['id_author']]) ?>"><?=$article['login'];?></a>
                                <h4>Дата публикации:</h4>
                                <?=$article['date_publication'];?>
                                <h4>Аннотация:</h4>
                                <?=$article['annotation'];?>
                            </div>
                            <?php if($article['id_author'] == Yii::$app->user->identity['id']):?>
                                <a href="<?= Url::to(['article/delete', 'id' => $article['id_article'],
                                    'typeArticle' => 'healthyFood']) ?>"
                                   class="btn btn-danger btn-view">Удалить</a>
                                <a href="<?= Url::to(['article/edit', 'id' => $article['id_article']]) ?>"
                                   class="btn btn-success btn-view">Редактировать</a>
                            <?php endif;?>
                            <a href="<?= Url::to(['article/view', 'id' => $article['id_article']]) ?>"
                               class="btn btn-success btn-view">Посмотреть</a>
                        </div>
                        </a>
                    </div>
                </div>
            <?php endforeach; ?>
        <hr/>
    </div>
</section>