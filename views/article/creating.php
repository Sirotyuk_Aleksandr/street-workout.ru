<?php
$this->title = 'Создание статьи';

use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
?>
<div class="container-fluid">
    <div class="col-md-6 col-md-offset-3">
        <h1 align="center">Создание статьи</h1>
        <?php
        $form = ActiveForm::begin(['class' => 'form-horizontal', 'id' => 'registration-form']);
        ?>
        <?= $form->field($creatingForm, 'name_article'); ?>
        <?= $form->field($creatingForm, 'main_image')->fileInput(); ?>
        <?= $form->field($creatingForm, 'annotation')->textarea()
            ->textInput(['placeholder' => 'Введите текст аннотации, то есть краткое содержание статьи']); ?>
        <?= $form->field($creatingForm, 'content')->widget(CKEditor::className(),[
            'editorOptions' => [
                'preset' => 'full',
                'inline' => false,
            ],
        ]);; ?>
        <?= $form->field($creatingForm, 'is_advertising')->checkbox(); ?>
        <div>
            <button type="submit" class="btn btn-primary">Создать статью</button>
        </div>
        <?php
        ActiveForm::end();
        ?>
    </div>
</div>
