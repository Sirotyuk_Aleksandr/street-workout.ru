<?php
$this->title = 'Редактирование статьи';

use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
?>
<div class="container-fluid">
    <div class="col-md-6 col-md-offset-3">
        <h1 align="center">Редактирование статьи</h1>
        <?php
        $form = ActiveForm::begin(['class' => 'form-horizontal', 'id' => 'registration-form']);
        ?>
        <?= $form->field($articleEditingForm, 'name_article'); ?>
        <?= $form->field($articleEditingForm, 'main_image')->fileInput(); ?>
        <?= $form->field($articleEditingForm, 'annotation')->textarea()
            ->textInput(['placeholder' => 'Введите текст аннотации, то есть краткое содержание статьи']); ?>
        <?= $form->field($articleEditingForm, 'content')->widget(CKEditor::className(),[
            'editorOptions' => [
                'preset' => 'full',
                'inline' => false,
            ],
        ]);; ?>
        <?= $form->field($articleEditingForm, 'is_advertising')->checkbox(); ?>
        <div>
            <button type="submit" class="btn btn-primary">Редактировать статью</button>
        </div>
        <?php
        ActiveForm::end();
        ?>
    </div>
</div>