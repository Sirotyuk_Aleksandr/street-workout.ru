<?php

$this->title = $challenge[0]['name_challenge'];
$this->registerMetaTag(['name' => 'keywords', 'content' => $challenge['name_challenge']]);

use yii\helpers\Url;
use yii\bootstrap\Modal;
?>

<section class="challenge">
    <div class="container-fluid">
        <div class="row">
            <?php if(!empty($challenge)): ?>
                <iframe width="560" height="350" src="<?=$challenge[0]['video_reference'];?>"
                        frameborder="0" allow="accelerometer; autoplay;
                                                encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
        </div>
        <div class="row">
            <span class="challenge-block">
                <span class="headers_3"><?=$challenge[0]['name_challenge']; ?></span> <br/>
                    <span class="headers_4">Создатель:</span> <br/>
                <a href="<?= Url::to(['/site/view-user', 'id' => $challenge[0]['id_creator']]) ?>">
                    <?= $challenge[0]['login']; ?></a> <br/>
                    <span class="headers_4">Описание:</span> <br/>
                <?= $challenge[0]['description']; ?> <br/>
                <a href="#" onclick="return takePartChallenge(<?=$challenge[0]['id_challenge'];?>);"
                   class="btn btn-success">Принять участие</a> <br/>
                   <span class="headers_4">Участники:</span> <br/>
            </span>
            <?php endif; ?>
        </div>
            <div class="challenge_participation">
                <table width="70%" cellpadding="5" border="1">
                    <tr>
                        <th>Логин</th>
                        <th>Дата создания</th>
                        <th>Видео</th>
                    </tr>
                    <?php
                    if(!empty($challengeParticipations))
                        foreach ($challengeParticipations as $challengeParticipation):
                            ?>
                            <tr>
                                <td><a href="<?= Url::to(['/site/view-user',
                                        'id' => $challengeParticipation['id_participant']]) ?>">
                                        <?=$challengeParticipation['login'];?></a></td>
                                <td><?=$challengeParticipation['date_participation'];?></td>
                                <td><iframe width="190" height="120" src="<?=$challengeParticipation['video_reference'];?>"
                                            frameborder="0" allow="accelerometer; autoplay;
                                                encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen></iframe></td>
                            </tr>
                        <?php
                        endforeach;
                    ?>
                </table>
            </div>
        </div>
</section>
<?php
Modal::begin([
    'header' => '<h2>Принять челлендж</h2>',
    'footer' => '<button type="button" data-dismiss="modal" class="btn btn-success"> Закрыть </button>',
    'id' => 'modal-challenge-taking-part',
]);
Modal::end();
?>


