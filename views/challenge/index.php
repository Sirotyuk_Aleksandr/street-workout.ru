<?php

$this->title = 'Челленджи';
use yii\helpers\Url;
use yii\widgets\Pjax;
?>
<?php Pjax::begin([
    'timeout' => 5000,
    'clientOptions' => ['method' => 'POST'],
    'enablePushState' => false,
]); ?>
    <section class="challenges">
        <div class="container-fluid">
            <h3 align="center">Список челленджей
            <?php if(!Yii::$app->user->isGuest): ?>
                | <a href="<?= Url::to(['/challenge/creating']) ?>">Создать челлендж</a>
            <?php endif; ?>
            </h3>
            <div class="challenge-block">
                <table width="100%" cellpadding="5" border="1">
                    <tr>
                        <th>Название</th>
                        <th>Дата создания</th>
                        <th>Создатель</th>
                        <th>Видео</th>
                        <th>Описание</th>
                        <th></th>
                    </tr>
                    <?php
                    if(!empty($challenges))
                        foreach ($challenges as $challenge):
                                ?>
                                <tr>
                                    <td><?=$challenge['name_challenge'];?></td>
                                    <td><?=(new DateTime($challenge['date_creation']))->format('d.m.Y'); ?></td>
                                    <td><a href="<?= Url::to(['site/view-user', 'id' => $challenge['id_creator']]) ?>">
                                            <?=$challenge['login'];?></a></td>
                                    <td><iframe width="190" height="120" src="<?=$challenge['video_reference'];?>"
                                                frameborder="0" allow="accelerometer; autoplay;
                                                encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe></td>
                                    <td><?=$challenge['description'];?></td>
                                    <td><a data-pjax="0" href="<?= Url::to(['/challenge/view',
                                            'id' => $challenge['id_challenge']]) ?>"
                                           class="btn btn-success">Подробнее / Принять участие</a></td>
                                </tr>
                            <?php
                        endforeach;
                    ?>
                </table>
            </div>
        </div>
    </section>
<?php Pjax::end();?>