<?php
$this->title = 'Принять участие';

use yii\widgets\ActiveForm;

?>
<div class="container-fluid">
    <?php
    $form = ActiveForm::begin(['class' => 'form-horizontal', 'id' => 'registration_form']);
    ?>
    <?= $form->field($participantForm, 'video_reference'); ?>
    <div>
        <button type="submit" class="btn btn-primary">Принять участие</button>
    </div>
    <?php
    ActiveForm::end();
    ?>
</div>
