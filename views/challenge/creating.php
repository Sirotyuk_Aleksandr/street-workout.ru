<?php
$this->title = 'Создание челленджа';

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
?>
<?php Pjax::begin([
    'timeout' => 5000,
    'clientOptions' => ['method' => 'POST'],
    'enablePushState' => false,
]); ?>
    <div class="container-fluid">
        <h3 align="center"><a href="<?= Url::to(['/challenge/index']) ?>">Список челленджей</a>
            | Создать челлендж</h3>
            <div class="col-md-5 col-md-offset-4 form-creating">
                <?php
                $form = ActiveForm::begin(['class' => 'form-horizontal', 'id' => 'registration-form']);
                ?>
                <?= $form->field($creatingForm, 'name_challenge')
                    ->textInput(['placeholder' => 'Введите название челленджа, например "жим лежа 100x100"']); ?>
                <?= $form->field($creatingForm, 'video_reference')
                    ->textInput(['placeholder' => 'Введите ссылку на Youtube-видео']); ?>
                <?= $form->field($creatingForm, 'description'); ?>
                <div>
                    <button type="submit" class="btn btn-primary">Создать челлендж</button>
                </div>
                <?php
                ActiveForm::end();
                ?>
            </div>
    </div>
<?php Pjax::end();?>