<?php
$this->title = 'Смена пароля';

?>
<div class="container-fluid">
    <?php if($statusMail):?>
        <h3 align="center">Для смены пароля прочтите инструкцию, высланную Вам на почту.</h3>
    <?php endif;?>
    <?php if(!$statusMail):?>
        <h3 align="center">
            Для того, чтобы сменить пароль, необходимо сначала подтвердить почту.<br/>
            После регистрации Вам должно было прийти письмо подтвеждения почты.
        </h3>
    <?php endif;?>
</div>