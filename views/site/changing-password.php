<?php
$this->title = 'Смена пароля';

use yii\widgets\ActiveForm;
?>
<div class="container-fluid">
    <div class="col-md-4 col-md-offset-4">
        <h1 align="center">Смена пароля</h1>

        <?php
        $form = ActiveForm::begin(['class' => 'form-horizontal', 'id' => 'authorization-form']);
        ?>
        <?= $form->field($changingPasswordForm, 'login'); ?>
        <?= $form->field($changingPasswordForm, 'password')->passwordInput(); ?>
        <?= $form->field($changingPasswordForm, 'newPassword')->passwordInput(); ?>
        <?= $form->field($changingPasswordForm, 'newRepeatPassword')->passwordInput(); ?>
        <div>
            <button type="submit" class="btn btn-primary">Сменить пароль</button>
        </div>
        <?php
        ActiveForm::end();
        ?>
    </div>
</div>