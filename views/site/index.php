<?php

$this->title = 'Главная';

use yii\helpers\Html;
use yii\helpers\Url;
?>

<!-- Slider -->
<section id="slider">
    <div id="slider_container" class="owl-carousel owl-theme">
        <?= Html::img('@web/images/1-slider.jpg', ['alt' => 'slide1', 'height' => '580px']); ?>
        <?= Html::img('@web/images/2-slider.jpg', ['alt' => 'slide2', 'height' => '580px']); ?>
        <?= Html::img('@web/images/3-slider.jpg', ['alt' => 'slide3', 'height' => '580px']); ?>
    </div>
</section>
<!-- End of slider -->

<!-- Latest news -->
<section class="latest-news">
    <h2>Что такое Street Workout?</h2>
    <div class="container-fluid">
        <div class="col-sm-4">
				<span class="news-block">
                    <?= Html::img('@web/images/1-latest_news.jpg', ['alt' => 'latest_news1', 'height' => '180px', 'width' => '280px']); ?>
					<p>
						Зародился Street Workout в Америке в 90-х годах, когда темнокожие подростки активно
                        работали над построением своего тела. В России Workout получил широкое распространение
                        в 2009 году.
					</p>
				</span>
        </div>
        <div class="col-sm-4">
				<span class="news-block">
					 <?= Html::img('@web/images/2-latest_news.jpg', ['alt' => 'latest_news2', 'height' => '180px', 'width' => '280px']); ?>
                    <p>
						Street Workout - это любительское спортивное направление. Включает в себя выполнение
                        различных упражнений на уличных спортплощадках.
					</p>
				</span>
        </div>
        <div class="col-sm-4">
				<span class="news-block">
					 <?= Html::img('@web/images/3-latest_news.jpg', ['alt' => 'latest_news3', 'height' => '180px', 'width' => '280px']); ?>
						<p>
                        Спортсмены могут выполнять различные элементы на турниках, брусьях, шведских стенках,
                        рукоходах и прочих конструкциях или вообще без их использования (на земле).
					</p>
				</span>
        </div>
    </div>
</section>
<!-- End of latest news -->

<!-- Popular tutorials -->
<section class="popular_tutorials">
    <h2>Популярные обучалки</h2>
    <div class="container-fluid">
        <?php foreach ($articles as $article): ?>
            <div class="col-md-6">
                <h3><?= $article['name']; ?></h3>
                <div class="video_tutorial">
                    <iframe width="560" height="315"
                            src="https://www.youtube.com/embed/<?= $article['video_link']; ?>" frameborder="0"
                            allow="autoplay; encrypted-media" allowfullscreen>
                    </iframe>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>
<!-- End of popular tutorials -->

