<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="fightOffers">
    <table width="100%" border="1">
        <th>Логин пользователя</th>
        <th>Город</th>
        <th>Адрес площадки</th>
        <th>Дата зарубы</th>
        <th>Упражнения</th>
        <th></th>
        <?php if(!empty($ownOffers))
            foreach ($ownOffers as $ownOffer):
                ?>
                <tr>
                    <td><?= $ownOffer['login']?></td>
                    <td><?= $ownOffer['city']?></td>
                    <td><?= $ownOffer['playground_address']?></td>
                    <td><?= $ownOffer['date_fight']?></td>
                    <td>
                        <?php
                        foreach ($exercises as $exercise)
                            if($ownOffer['id_offer'] == $exercise['id_fight_offer'])
                                echo $exercise['name_exercise']."<br/>";
                        ?>
                    </td>
                    <td><i data-id="<?= $ownOffer['id_offer'] ?>" class="fa fa-times delete-ownFightOffer" aria-hidden="true"></i></td>
                </tr>
            <?php endforeach; ?>
    </table>
</div>
