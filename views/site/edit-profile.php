<?php
$this->title = 'Редактирование профиля';

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
?>
<div class="container-fluid">
    <div class="col-md-4 col-md-offset-4">
        <h1 align="center">Редактирование профиля</h1>

        <?php
        $form = ActiveForm::begin(['class' => 'form-horizontal', 'id' => 'registration-form']);
        ?>
        <?= $form->field($editProfileForm, 'last_name'); ?>
        <?= $form->field($editProfileForm, 'name'); ?>
        <?= $form->field($editProfileForm, 'patronymic'); ?>
        <?= $form->field($editProfileForm, 'image_avatar')->fileInput(); ?>
        <?= $form->field($editProfileForm, 'date_birth')->widget(DatePicker::className(), [
            'type' => DatePicker::TYPE_INPUT,
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]
        ]); ?>
        <?= $form->field($editProfileForm, 'experience')
            ->textInput(['placeholder' => 'Введите количество полных лет (одно число), например 4']); ?>
        <?= $form->field($editProfileForm, 'growth')
            ->textInput(['placeholder' => 'Введите одно число, например 172']); ?>
        <?= $form->field($editProfileForm, 'weight')
            ->textInput(['placeholder' => 'Введите одно число, например 67']); ?>
        <?= $form->field($editProfileForm, 'max_pulling_up')
            ->textInput(['placeholder' => 'Введите одно число, например 15']); ?>
        <?= $form->field($editProfileForm, 'max_push_up_bars')
            ->textInput(['placeholder' => 'Введите одно число, например 30']); ?>
        <?= $form->field($editProfileForm, 'max_push_up')
            ->textInput(['placeholder' => 'Введите одно число, например 55']); ?>
        <?= $form->field($editProfileForm, 'max_bench_press')
            ->textInput(['placeholder' => 'Введите одно число, например 100']); ?>
        <?= $form->field($editProfileForm, 'reference_youtube')
            ->textInput(['placeholder' => 'Введите ссылку на Ваш аккаунт в Youtube']); ?>
        <?= $form->field($editProfileForm, 'reference_vk')
            ->textInput(['placeholder' => 'Введите ссылку на Ваш аккаунт в VKontakte']); ?>
        <div>
            <button type="submit" class="btn btn-primary">Редактировать профиль</button>
            <a class="btn btn-success" href="<?= Url::to(['/site/send-mail-change-password']) ?>">Сменить пароль</a>
        </div>
        <?php
        ActiveForm::end();
        ?>
    </div>
</div>