<?php
$this->title = $user['login'];

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="container-fluid">
    <div class="profile_block">
        <?= Html::img("@web/images/uploads/profile/{$user['image_avatar']}",
            ['alt' => Yii::$app->user->identity['login'],
                'height' => '90px', 'width' => '100px', 'id' => 'avatar_img']); ?>
        <h2 align="center"><?=$user['login'];?></h2>
        ФИО:
        <?=$user['last_name'];?>
        <?=$user['name'];?>
        <?=$user['patronymic'];?> <br/>
        Дата рождения:  <?=$user['date_birth'];?><br/>
        Стаж: <?=$user['experience'];?> года<br/>
        Рост: <?=$user['growth'];?><br/>
        Вес: <?=$user['weight'];?><br/>
        Максимальное количество подтягиваний: <?=$user['max_pulling_up'];?><br/>
        Максимальное количество отжиманий на брусьях: <?= $user['max_push_up_bars']; ?><br/>
        Максимальное количество отжиманий от пола: <?=$user['max_push_up']; ?><br/>
        Максимальный веса штанги при жиме лежа: <?=$user['max_bench_press']; ?><br/>
        <div class="profile_icon">
            <a href="<?= $user['reference_vk']; ?>">
                <i class="fa fa-vk fa-5x profile_icon_vk" aria-hidden="true"></i>
            </a>
            <a href="<?= $user['reference_youtube']; ?>">
                <i class="fa fa-youtube fa-5x profile_icon_youtube" aria-hidden="true"></i>
            </a>
        </div>
        <?php if(!Yii::$app->user->isGuest): ?>
            <a id="offer_fight_btn" href="#" class="btn btn-success" onclick="return offerFight(<?=$user['id'];?>);">
                Вызвать на зарубу
            </a><br/>
        <?php endif; ?>
    </div>
</div>

<?php
Modal::begin([
    'header' => '<h2>Вызвать на зарубу</h2>',
    'footer' => '<button type="button" data-dismiss="modal" class="btn btn-success"> Закрыть </button>',
    'id' => 'modal-fight',
]);
Modal::end();

