<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="fightOffers">
    <table width="100%" border="1">
        <th>Логин пользователя</th>
        <th>Город</th>
        <th>Адрес площадки</th>
        <th>Дата зарубы</th>
        <th>Упражнения</th>
        <th></th>
        <th></th>
        <?php if(!empty($offers))
                foreach ($offers as $offer):
        ?>
        <tr>
            <td><?= $offer['login']?></td>
            <td><?= $offer['city']?></td>
            <td><?= $offer['playground_address']?></td>
            <td><?= $offer['date_fight']?></td>
            <td>
                <?php
                foreach ($exercises as $exercise)
                    if($offer['id_offer'] == $exercise['id_fight_offer'])
                        echo $exercise['name_exercise']."<br/>";
                ?>
            </td>
            <td>
                <i data-id="<?= $offer['id_offer'] ?>" class="fa fa-plus accept-fight" aria-hidden="true"></i>
            </td>
            <td>
                <i data-id="<?= $offer['id_offer'] ?>" class="fa fa-times delete-fightOffer" aria-hidden="true"></i>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
</div>

