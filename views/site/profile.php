<?php
    $this->title = 'Личный кабинет';

    use yii\bootstrap\Modal;
    use yii\helpers\Html;
    use yii\helpers\Url;
?>

<div class="container-fluid">
    <div class="profile_block">
        <?= Html::img("@web/images/uploads/profile/{$imageName}",
            ['alt' => Yii::$app->user->identity['login'],
            'height' => '90px', 'width' => '100px', 'id' => 'avatar_img']); ?>
        <h2 align="center"><?=Yii::$app->user->identity['login']; ?></h2>
        ФИО:
        <?= Yii::$app->user->identity['last_name']; ?>
        <?= Yii::$app->user->identity['name']; ?>
        <?= Yii::$app->user->identity['patronymic']; ?><br/>
        Дата рождения:  <?= Yii::$app->user->identity['date_birth']; ?><br/>
        Стаж: <?= Yii::$app->user->identity['experience']; ?> года<br/>
        Рост: <?= Yii::$app->user->identity['growth']; ?><br/>
        Вес: <?= Yii::$app->user->identity['weight']; ?><br/>
        Максимальное количество подтягиваний: <?= Yii::$app->user->identity['max_pulling_up']; ?><br/>
        Максимальное количество отжиманий на брусьях: <?= Yii::$app->user->identity['max_push_up_bars']; ?><br/>
        Максимальное количество отжиманий от пола: <?= Yii::$app->user->identity['max_push_up']; ?><br/>
        Максимальный веса штанги при жиме лежа: <?= Yii::$app->user->identity['max_bench_press']; ?><br/>
        Прочитанные книги: <a href="#" onclick="return getBooks();">открыть список</a><br/>
        Изученные элементы: <a href="#" onclick="return getElements();">открыть список</a><br/>
        Предложения о зарубах: <a href="#" onclick="return getFightOffers();">открыть список</a><br/>
        Мои предложения о зарубах: <a href="#" onclick="return getOwnFightOffers();">открыть список</a><br/>
        <div class="profile_icon">
            <a href="<?= Yii::$app->user->identity['reference_vk']; ?>">
                <i class="fa fa-vk fa-5x profile_icon_vk" aria-hidden="true"></i>
            </a>
            <a href="<?= Yii::$app->user->identity['reference_youtube'];?>">
                <i class="fa fa-youtube fa-5x profile_icon_youtube" aria-hidden="true"></i>
            </a>
        </div>
    </div>
</div>
<?php
    Modal::begin([
        'header' => '<h2>Прочитанные книги</h2>',
        'footer' => '<button type="button" data-dismiss="modal" class="btn btn-success"> Закрыть </button>',
        'id' => 'modal-books',
    ]);
    Modal::end();

    Modal::begin([
        'header' => '<h2>Изученные элементы</h2>',
        'footer' => '<button type="button" data-dismiss="modal" class="btn btn-success"> Закрыть </button>',
        'id' => 'modal-elements',
    ]);
    Modal::end();

    Modal::begin([
        'header' => '<h2>Предложения о зарубах</h2>',
        'footer' => '<button type="button" data-dismiss="modal" class="btn btn-success"> Закрыть </button>',
        'size' => Modal::SIZE_LARGE,
        'id' => 'modal-fightOffers',
    ]);
    Modal::end();

    Modal::begin([
        'header' => '<h2>Мои предложения о зарубах</h2>',
        'footer' => '<button type="button" data-dismiss="modal" class="btn btn-success"> Закрыть </button>',
        'size' => Modal::SIZE_LARGE,
        'id' => 'modal-fightOwnOffers',
    ]);
    Modal::end();
?>


