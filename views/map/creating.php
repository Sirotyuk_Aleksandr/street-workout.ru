<?php
$this->title = 'Создание спортивного объекта';

use yii\widgets\ActiveForm;
?>
<div class="container-fluid">
    <h3 align="center">Создание спортивного объекта на карте</h3>
    <div class="col-md-5 col-md-offset-4 form-creating">
        <?php
        $form = ActiveForm::begin(['class' => 'form-horizontal', 'id' => 'registration-form']);
        ?>
        <?= $form->field($creatingForm, 'name_facility')
            ->textInput(['placeholder' => 'Введите название объекта, например "Современная Workout-площадка"']); ?>
        <?= $form->field($creatingForm, 'address')
            ->textInput(['placeholder' => 'Введите адрес объекта, например парк Пушкина или ул.Ленина 34']); ?>
        <?= $form->field($creatingForm, 'main_image')->fileInput(); ?>
        <?= $form->field($creatingForm, 'latitude')->textInput(['value' => $_COOKIE['lat'],
            'placeholder' =>'Введите широту расположения объекта, например 55.161017']); ?>
        <?= $form->field($creatingForm, 'longitude')->textInput(['value' => $_COOKIE['lng'],
            'placeholder' => 'Введите долготу расположения объекта, например 61.327743']); ?>
        <?= $form->field($creatingForm, 'description')->textarea(['placeholder' => 'Необязательное поле']); ?>
        <div>
            <button type="submit" class="btn btn-primary">Создать объект</button>
        </div>
        <?php
        ActiveForm::end();
        ?>
    </div>
</div>