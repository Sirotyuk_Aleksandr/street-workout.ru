<?php

$this->title = $sportsObject[0]['name_facility'];
$this->registerMetaTag(['name' => 'keywords', 'content' => $sportsObject[0]['name_facility']]);

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>

<section class="sports_facility">
    <div class="container-fluid">
        <div class="row">
            <?php if(!empty($sportsObject)): ?>
            <div class="col-md-3 col-md-offset-2">
                <?= Html::img("@web/images/uploads/sports_objects/{$sportsObject[0]['main_image']}",
                    ['alt' => $sportsObject[0]['name_facility'], 'height' => '240px', 'width' => '240px', 'id' => 'img-book']); ?>
            </div>
            <div class="col-md-4">
                <span class="facility-block">
                    <span class="headers_3"><?=$sportsObject[0]['name_facility']  ?></span> <br/>
                        <span class="headers_4">Автор:</span>
                    <a href="<?= Url::to(['/site/view-user', 'id' => $sportsObject[0]['id_author']]) ?>">
                        <?= $sportsObject[0]['login']; ?></a><br/>
                        <span class="headers_4">Описание:</span><br/>
                    <?= $sportsObject[0]['description']; ?><br/>
                    <?php if(!Yii::$app->user->isGuest): ?>
                        <span class="headers_4">Средняя оценка:</span>
                        <?= $averageMarkFacility; ?><br/>
                        <span class="headers_4">Оценить:</span>
                        <a class="rate_facility" data-id="<?=$sportsObject[0]['id_facility']?>" data-mark="1" href="<?= Url::to(['map/rate-facility',
                            'id' => $sportsObject[0]['id_facility'], 'mark' => 1]) ?>">☆</a>
                        <a class="rate_facility" data-id="<?=$sportsObject[0]['id_facility']?>" data-mark="2" href="<?= Url::to(['map/rate-facility',
                            'id' => $sportsObject[0]['id_facility'], 'mark' => 2]) ?>">☆</a>
                        <a class="rate_facility" data-id="<?=$sportsObject[0]['id_facility']?>" data-mark="3" href="<?= Url::to(['map/rate-facility',
                            'id' => $sportsObject[0]['id_facility'], 'mark' => 3]) ?>">☆</a>
                        <a class="rate_facility" data-id="<?=$sportsObject[0]['id_facility']?>" data-mark="4" href="<?= Url::to(['map/rate-facility',
                            'id' => $sportsObject[0]['id_facility'], 'mark' => 4]) ?>">☆</a>
                        <a class="rate_facility" data-id="<?=$sportsObject[0]['id_facility']?>" data-mark="5" href="<?= Url::to(['map/rate-facility',
                            'id' => $sportsObject[0]['id_facility'], 'mark' => 5]) ?>">☆</a><br/>
                    <?php endif; ?>
                </span>
            </div>
        </div>

            <div class="row">
                <div class="col-md-3 col-md-offset-2">
                    <?php if(!Yii::$app->user->isGuest): ?>
                        <div>
                            <span class="headers_3">Написать отзыв:</span>
                            <?php
                            $form = ActiveForm::begin(['class' => 'form-horizontal', 'id' => 'comment-form']);
                            ?>
                            <?= $form->field($commentForm, 'text')->textarea(); ?>
                            <div>
                                <button type="submit" class="btn btn-primary">Отправить</button>
                            </div>
                            <?php
                            ActiveForm::end();
                            ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

            <div class="row">
                <div class="reviews">
                    <div class="col-md-4 col-md-offset-2">
                        <span class="headers_3">Отзывы:</span> <br/>
                        <?php
                        if(!empty($commentFacility))
                            foreach ($commentFacility as $commentFacility){
                                echo '<span class="headers">'.$commentFacility['login'].'</span>'.' : ';
                                echo $commentFacility['content'].'<br/>';
                            }
                        ?>
                    </div>
                </div>
            </div>

    </div>
        <?php endif; ?>
</section>