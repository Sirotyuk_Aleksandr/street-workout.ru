<?php

$this->title = 'Карта площадок';

use yii\helpers\Url;
use yii\helpers\Html;
?>

<?php if (isset($_COOKIE['lat']) && isset($_COOKIE['lng'])) {
    $lat = $_COOKIE["lat"];
    $lng = $_COOKIE["lng"];
}?>
<h3 align="center">Карта площадок и тренажерных залов</h3>
<h4><a class="btn btn-success" target="_blank" href="<?= Url::to(['/map/creating']) ?>">Создать объект на карте</a></h4>
Координаты для создания объекта на карте:
<span id="clicked_element">отсутствуют (нажмите на необходимую точку на карте)</span>

<script src="https://maps.api.2gis.ru/2.0/loader.js?pkg=full"></script>

<div id="map" style="width:100%; height:500px"></div>

<script type="text/javascript">
    var map,
        clickedElement = document.getElementById('clicked_element');

    DG.then(function () {
        map = DG.map('map', {
            center: [55.15402, 61.42915],
            zoom: 11
        });

        map.locate({setView: true, watch: true})
            .on('locationfound', function(e) {
                DG.marker([e.latitude, e.longitude]).addTo(map);
            })
            .on('locationerror', function(e) {
                DG.popup()
                    .setLatLng(map.getCenter())
                    .setContent('Доступ к определению местоположения отключён')
                    .openOn(map);
            });

        map.on('click', function(e) {
            clickedElement.innerHTML = e.latlng.lat + ', ' + e.latlng.lng;
            document.cookie = "lat=" + e.latlng.lat
                + "; path=/map/creating";
            document.cookie = "lng=" + e.latlng.lng
                + "; path=/map/creating";
        });
        <?php
            foreach ($sportsObjects as $sportsObject):
        ?>
            DG.marker([<?= $sportsObject['latitude'];?>,
                <?= $sportsObject['longitude'];?>]).addTo(map)
                .bindPopup('<?= Html::img("@web/images/uploads/sports_objects/{$sportsObject['main_image']}",
                    ['alt' => $sportsObject['name_facility'], 'height' => '90px', 'width' => '120px', 'class' => 'map_img']);?>'+
                    '<p class="map_description" align="center"><?= $sportsObject['name_facility'];?>'+
                    '<br/>Создатель метки: '+
                    '<a href="<?=Url::to(['/site/view-user', 'id' => $sportsObject['id_author']]);?>">'+
                    '<?= $sportsObject['login']; ?></a>'+
                    '<br/>Адрес: <?= $sportsObject['address'];?>'+
                    '<br/><a href="<?= Url::to(['/map/view', 'id' => $sportsObject['id_facility']])?>">Подробнее</a></p>')
                .bindLabel('<?= $sportsObject['name_facility'];?> <br/><?= $sportsObject['address'];?>');
                <?php endforeach;?>
        DG.control.location().addTo(map);
    });
</script>
