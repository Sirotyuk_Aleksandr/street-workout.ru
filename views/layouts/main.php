<?php
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <script src="http://api-maps.yandex.ru/2.0/?load=package.full&apikey=f1db1d62-8e17-4848-b69f-2fbae8ffd06f&lang=ru-RU"
            type="text/javascript"></script>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<!-- Header with the top menu -->
<header>
    <nav class="top-menu">
        <div class="container-fluid">
            <ul class="top-menu">
                <li><?= Html::img('@web/images/main_logo.jpg', ['alt' => 'logo_workout', 'width' => '30%']); ?></li>
                <li><a href="<?= Url::home(); ?>">Главная</a></li>
                <li class="dropdown-standart"><a href="" class="dropdown">Соревнования</a>
                    <ul class="submenu-standart">
                        <li><a href="<?= Url::to(['/competition/index']) ?>">Список соревнований</a></li>
                        <li><a href="<?= Url::to(['/fights/upcoming-fights']) ?>">Workout - зарубы</a></li>
                        <li><a href="<?= Url::to(['/challenge/index']) ?>">Workout - челенджи</a></li>
                    </ul>
                </li>
                <li><a class="link_submenu" href="">Статьи</a>
                    <ul class="submenu">
                        <li><h3>Тренировки</h3>
                            <ul>
                                <li><a href="<?= Url::to(['/article/index']) ?>">Обучающие статьи</a></li>
                                <li><a href="<?= Url::to(['/elements']) ?>">Элементы воркаута</a></li>
                            </ul>
                        </li>
                        <li><h3>Спортивная жизнь</h3>
                            <ul>
                                <li><a href="<?= Url::to(['/article/index-food']) ?>">Здоровое питание</a></li>
                                <li><a href="<?= Url::to(['/book/index']) ?>">Книги для саморазвития</a></li>
                            </ul>
                        </li>
                        <li>
                            <ul>
                                <li>
                                    <!-- 			            	 <li><h3 class="image-header">Статьи</h3></li> -->
                                    <?= Html::img('@web/images/submenu.jpg', ['alt' => 'submenu_workout']); ?>
                                </li>
                            </ul>
                        </li>
                    </ul>
                <li><a href="<?= Url::to(['/map/index']) ?>">Карта площадок</a>
                </li>
                <li><a href="<?= Url::to(['/feedback/index']) ?>">Обратная связь</a></li>
                <?php if(!Yii::$app->user->isGuest): ?>
                    <li class="dropdown-standart"><a href="" class="dropdown"><?=Yii::$app->user->identity['login']; ?></a>
                        <ul class="submenu-standart">
                            <li><a href="<?= Url::to(['/profile']) ?>">Личный кабинет</a></li>
                            <li><a href="<?= Url::to(['/profile/edit']) ?>">Редактировать профиль</a></li>
                            <li><a href="<?= Url::to(['/site/logout']) ?>">Выйти из системы</a></li>
                        </ul>
                    </li>
                <?php else: ?>
                    <li><a href="<?= Url::to(['/authorization']) ?>">Войти</a></li>
                <?php endif; ?>
            </ul>
        </div>
    </nav>
</header>
<!-- End of header with the top menu -->
<div class="main_content">
    <?= $content; ?>
</div>
<!-- Footer -->
<footer>
    <div class="container-fluid">
        <div class="first_column">
            <div class="col-md-4">
                <h2>Street Workout и саморазвитие</h2>
                <ul>
                    <li>Все права защищены.</li>
                    <li>@ 2019 <a href="<?= Url::home(); ?>">street-workout.ru</a></li>
                </ul>
            </div>
        </div>
        <div class="second_column">
            <div class="col-md-4">
                <h2>Карта сайта</h2>
                <ul>
                    <table width="100%" cellpadding="5">
                        <tr>
                            <td>
                                <li><a href="<?= Url::home(); ?>">Главная</a></li>
                                <li><a href="<?= Url::to(['/profile']) ?>">Личный кабинет</a></li>
                                <li><a href="<?= Url::to(['/competition/index']) ?>">Список соревнований</a></li>
                                <li><a href="<?= Url::to(['/fights/upcoming-fights']) ?>">Workout - зарубы</a></li>
                            </td>
                            <td>
                                <li><a href="<?= Url::to(['/challenge/index']) ?>">Workout - челенджи</a></li>
                                <li><a href="<?= Url::to(['/book/index']) ?>">Книги для саморазвития</a></li>
                                <li><a href="<?= Url::to(['/elements']) ?>">Элементы воркаута</a></li>
                                <li><a href="<?= Url::to(['/moderation']) ?>">Модерация</a></li>
                            </td>
                        </tr>
                    </table>
                </ul>
            </div>
        </div>
        <div class="third_column">
            <div class="col-md-4">
                <h2>Разработчики</h2>
                <ul>
                    <li>Веб-разработка: Александр Сиротюк</li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- End of footer -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>