<?php

namespace app\controllers;

use Yii;
use app\models\SportsFacility;
use app\models\SportsFacilityCreatingForm;
use app\models\ImageUploading;
use app\models\SportsFacilityMark;
use app\models\CommentForm;
use yii\web\Controller;
use yii\web\UploadedFile;

class MapController extends Controller
{
    public function actionIndex()
    {
        $sportsFacilityModel = new SportsFacility();
        $sportsObjects = $sportsFacilityModel->getSportsObjects();

        return $this->render('index', compact('sportsObjects'));
    }

    public function actionView($id)
    {
        $sportsFacilityModel = new SportsFacility();
        $sportsObject = $sportsFacilityModel->getSportsObjectById($id);

        $averageMarkFacility = SportsFacilityMark::find()->where(['id_facility' => $id])->average('value');
        $averageMarkFacility = round($averageMarkFacility, 1);

        $commentForm = new CommentForm();
        if (isset($_POST['CommentForm'])) {
            $commentForm->attributes = Yii::$app->request->post('CommentForm');
            if ($commentForm->validate()) {
                $commentForm->writeFacilityComment($id);
            }
        }
        $commentFacility = $sportsFacilityModel->getFacilityComment($id);
        return $this->render('view', compact('sportsObject',
            'commentForm', 'commentFacility', 'averageMarkFacility'));
    }

    public function actionCreating()
    {
        $creatingForm = new SportsFacilityCreatingForm();
        $imageUploadingModel = new ImageUploading();

        if (isset($_POST['SportsFacilityCreatingForm'])) {
            $creatingForm->attributes = Yii::$app->request->post('SportsFacilityCreatingForm');

            $typeImage = 'sportsFacility';
            $file = UploadedFile::getInstance($creatingForm, 'main_image');
            $nameImage = $imageUploadingModel->uploadImage($file, $typeImage);
            $creatingForm->main_image = $nameImage;

            if ($creatingForm->validate() && $creatingForm->createFacility()) {
                return $this->redirect('/map/index');
            }
        }
        return $this->render('creating', compact('creatingForm'));
    }

    public function actionRateFacility($id, $mark)
    {
        $markFacility = SportsFacilityMark::findOne([
            'id_user' => Yii::$app->user->identity['id'],
            'id_facility' => $id
        ]);

        if (!isset($markFacility)) {
            $facilityMarkModel = new SportsFacilityMark();
            $facilityMarkModel->id_facility = $id;
            $facilityMarkModel->id_user = Yii::$app->user->identity['id'];
            $facilityMarkModel->value = $mark;
            $facilityMarkModel->save();
        } else {
            $markFacility->value = $mark;
            $markFacility->save();
        }

        return true;
    }
}