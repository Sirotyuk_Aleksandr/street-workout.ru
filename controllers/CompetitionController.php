<?php

namespace app\controllers;

use app\models\TypeCompetition;
use yii\web\Controller;
use app\models\Competition;
use app\models\CompetitionCreatingForm;
use Yii;
use yii\web\ForbiddenHttpException;

class CompetitionController extends Controller
{
    public function actionIndex()
    {
        $competition_model = new Competition();
        $competitions = $competition_model->getBrieflyCompetitions();
        return $this->render('index', compact('competitions'));
    }

    public function actionView($id)
    {
        $competition_model = new Competition();
        $competition = $competition_model->getCompetitionById($id);
        $awards = $competition_model->getAwardsById($id);
        $sponsors = $competition_model->getSponsorsById($id);
        return $this->render('view', compact('competition', 'awards', 'sponsors', 'participants'));
    }

    public function actionViewParticipants($id)
    {
        $competition_model = new Competition();
        $competition = $competition_model->getCompetitionById($id);
        $participants = $competition_model->getParticipantsById($id);
        return $this->render('view-participants', compact('participants', 'competition'));
    }

    public function actionCreating()
    {
        $flagProhibition = false;
        if (Yii::$app->user->can('createCompetition')) {
            $typesCompetitionsQuery = TypeCompetition::find()->select(['type_name'])->asArray()->all();
            for ($i = 0; $i < count($typesCompetitionsQuery); $i++) {
                $types_competitions[$i + 1] = $typesCompetitionsQuery[$i]['type_name'];
            }
            $creatingForm = new CompetitionCreatingForm();
            if (isset($_POST['CompetitionCreatingForm'])) {
                $creatingForm->attributes = Yii::$app->request->post('CompetitionCreatingForm');
                if ($creatingForm->validate() && $creatingForm->signUp()) {
                    return $this->redirect('/competitions');
                }
            }
            return $this->render('creating', compact('creatingForm',
                'flagProhibition', 'types_competitions'));
        } else {
            $flagProhibition = true;
            return $this->render('creating', compact('flagProhibition'));
        }
    }

    public function actionCompleted()
    {
        $competition_model = new Competition();
        $competitions = $competition_model->getBrieflyCompetitions();
        return $this->render('completed', compact('competitions'));
    }

    public function actionAddParticipant($id_competition)
    {
        $competition_model = new Competition();
        $competition_model->addParticipant($id_competition);
        return true;
    }
}