<?php

namespace app\controllers;

use Yii;
use app\models\FeedbackForm;
use yii\web\Controller;

class FeedbackController extends Controller
{
    public function actionIndex()
    {
        $feedbackForm = new FeedbackForm();
        if (isset($_POST['FeedbackForm'])) {
            $feedbackForm->attributes = Yii::$app->request->post('FeedbackForm');
            if ($feedbackForm->validate()) {
                $feedbackForm->sendComment();
            }
            $this->redirect('/feedback/index');
        }
        return $this->render('index', compact('feedbackForm'));
    }

}