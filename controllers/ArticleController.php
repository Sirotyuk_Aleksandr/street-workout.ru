<?php
/**
 * Created by PhpStorm.
 * User: Aleks
 * Date: 21.01.2019
 * Time: 19:00
 */

namespace app\controllers;

use app\models\ArticleEditingForm;
use app\models\ImageUploading;
use Yii;
use app\models\ArticleCreatingForm;
use yii\web\Controller;
use app\models\Article;
use yii\web\UploadedFile;

class ArticleController extends Controller
{
    public function actionIndex()
    {
        $articleModel = new Article();
        $articles = $articleModel->getArticles();
        return $this->render('index', compact('articles'));
    }

    public function actionView($id)
    {
        $articleModel = new Article();
        $article = $articleModel->getArticleById($id);
        return $this->render('view', compact('article'));
    }

    public function actionCreate($categoryId)
    {
        $creatingForm = new ArticleCreatingForm();
        $imageUploadingModel = new ImageUploading();

        if (isset($_POST['ArticleCreatingForm'])) {
            $creatingForm->attributes = Yii::$app->request->post('ArticleCreatingForm');

            $typeImage = 'article';
            $file = UploadedFile::getInstance($creatingForm, 'main_image');
            $nameImage = $imageUploadingModel->uploadImage($file, $typeImage);
            $creatingForm->main_image = $nameImage;

            if ($creatingForm->validate() && $creatingForm->createArticle($categoryId)) {
                if ($categoryId == 2) {
                    return $this->redirect('/articles');
                }
                if ($categoryId == 4) {
                    return $this->redirect('/articles/healthy-food');
                }
            }
        }
        return $this->render('creating', compact('creatingForm'));
    }

    public function actionIndexFood()
    {
        $articleModel = new Article();
        $articles = $articleModel->getFoodArticles();
        return $this->render('index-food', compact('articles'));
    }

    public function actionDelete($id, $typeArticle)
    {
        $article = Article::findOne($id);
        if ($article['id_author'] == Yii::$app->user->identity['id']) {
            $article->delete();
        }
        if ($typeArticle == 'tutorials') {
            $articleModel = new Article();
            $articles = $articleModel->getArticles();
            return $this->render('index', compact('articles'));
        }
        if ($typeArticle == 'healthyFood') {
            $articleModel = new Article();
            $articles = $articleModel->getFoodArticles();
            return $this->render('index-food', compact('articles'));
        }
        return true;
    }

    public function actionEdit($id)
    {
        $article = Article::findOne($id);
        $articleEditingForm = new ArticleEditingForm();

        if ($article['id_author'] == Yii::$app->user->identity['id']) {
            if (isset($_POST['ArticleEditingForm'])) {
                $articleEditingForm->attributes = Yii::$app->request->post('ArticleEditingForm');
                if ($articleEditingForm->validate() && $articleEditingForm->editArticle($article)) {
                    $url = '/article/view?id=' . $id;
                    $this->redirect($url);
                }
            }
            return $this->render('editing', compact('articleEditingForm'));
        } else {
            return $this->goHome();
        }
    }
}