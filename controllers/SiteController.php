<?php

namespace app\controllers;

use app\models\ChangingPasswordForm;
use app\models\FightOfferExercise;
use app\models\ImageUploading;
use Yii;
use app\models\EditProfileForm;
use app\models\FightOffer;
use app\models\RegistrationForm;
use app\models\User;
use app\models\Article;
use yii\web\Controller;
use app\models\AuthorizationForm;
use yii\web\UploadedFile;


class SiteController extends Controller
{
    public function actionIndex()
    {
        $articles = Article::find()->where(['id_category' => 1])->asArray()->all();
        return $this->render('index', compact('articles'));
    }

    public function actionRegistration()
    {
        $registrationForm = new RegistrationForm();
        if (isset($_POST['RegistrationForm']) && $_POST['RegistrationForm']['acceptingRules'] == 1) {
            $registrationForm->attributes = Yii::$app->request->post('RegistrationForm');
            $time = (string)time();
            $token = md5($time);
            if ($registrationForm->validate() && $registrationForm->signUp($token) &&
                $registrationForm->verifyUser($token)) {
                return $this->redirect('authorization');
            }
        }
        return $this->render('registration', compact('registrationForm'));
    }

    public function actionRules()
    {
        return $this->render('rules');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest)
            return $this->goHome();
        $authorizationForm = new AuthorizationForm();
        if (isset($_POST['AuthorizationForm'])) {
            $authorizationForm->attributes = Yii::$app->request->post('AuthorizationForm');
            if ($authorizationForm->validate()) {
                Yii::$app->user->login($authorizationForm->getUser());
                return $this->goHome();
            }
        }
        return $this->render('authorization', compact('authorizationForm'));
    }

    public function actionLogout()
    {
        if (!Yii::$app->user->isGuest) {
            Yii::$app->user->logout();
            return $this->goHome();
        }
    }

    public function actionProfile()
    {
        $user = new User();
        $books = $user->getBooksByUserId(Yii::$app->user->identity['id']);
        $elements = $user->getElementsByUserId(Yii::$app->user->identity['id']);
        $offers = FightOffer::find()->where(['id_second_user' => Yii::$app->user->identity['id']])->all();
        $ownOffers = FightOffer::find()->where(['id_first_user' => Yii::$app->user->identity['id']])->all();
        $imageName = Yii::$app->user->identity['image_avatar'];
        return $this->render('profile', compact('books', 'elements',
            'offers', 'ownOffers', 'imageName'));
    }

    public function actionEditProfile()
    {
        $editProfileForm = new EditProfileForm();
        $imageUploadingModel = new ImageUploading();

        if (isset($_POST['EditProfileForm'])) {
            $editProfileForm->attributes = Yii::$app->request->post('EditProfileForm');

            $typeImage = 'editingProfile';
            $file = UploadedFile::getInstance($editProfileForm, 'image_avatar');
            if (!empty($file)) {
                $nameImage = $imageUploadingModel->uploadImage($file, $typeImage);
                $editProfileForm->image_avatar = $nameImage;
            }

            if ($editProfileForm->validate() && $editProfileForm->edit()) {
                return $this->goHome();
            }
        }
        return $this->render('edit-profile', compact('editProfileForm'));
    }

    public function actionShowBooks()
    {
        $this->layout = false;
        $user = new User();
        $books = $user->getBooksByUserId(Yii::$app->user->identity['id']);
        return $this->render('book-cart', compact('books'));
    }

    public function actionDeleteBook($id)
    {
        $this->layout = false;
        $user = new User();
        $user->deleteBook($id);
        $books = $user->getBooksByUserId(Yii::$app->user->identity['id']);
        return $this->render('book-cart', compact('books'));
    }

    public function actionShowElements()
    {
        $this->layout = false;
        $user = new User();
        $elements = $user->getElementsByUserId(Yii::$app->user->identity['id']);
        return $this->render('window-elements', compact('elements'));
    }

    public function actionDeleteElement($id)
    {
        $this->layout = false;
        $user = new User();
        $user->deleteElement($id);
        $elements = $user->getElementsByUserId(Yii::$app->user->identity['id']);
        return $this->render('window-elements', compact('elements'));
    }

    public function actionViewUser($id)
    {
        $user = User::findOne($id);
        return $this->render('view-user', compact('user'));
    }

    public function actionShowFightOffers()
    {
        $this->layout = false;
        $user = new User();
        $fightOfferExercisesModel = new FightOfferExercise();

        $offers = $user->getFightOffersBySecondUserId(Yii::$app->user->identity['id']);
        $exercises = $fightOfferExercisesModel->getFightOfferExercises();
        return $this->render('window-fightOffers', compact('offers', 'exercises'));
    }

    public function actionDeleteFightOffer($id)
    {
        $this->layout = false;
        $user = new User();
        $user->deleteFightOfferById($id);
        $offers = $user->getFightOffersBySecondUserId(Yii::$app->user->identity['id']);

        $fightOfferExercisesModel = new FightOfferExercise();
        $exercises = $fightOfferExercisesModel->getFightOfferExercises();

        return $this->render('window-fightOffers', compact('offers', 'exercises'));
    }

    public function actionAcceptFight($id)
    {
        $this->layout = false;
        $user = new User();
        $user->acceptOfferById($id);
        $offers = $user->getFightOffersBySecondUserId(Yii::$app->user->identity['id']);

        $fightOfferExercisesModel = new FightOfferExercise();
        $exercises = $fightOfferExercisesModel->getFightOfferExercises();
        return $this->render('window-fightOffers', compact('offers', 'exercises'));
    }

    public function actionShowOwnFightOffers()
    {
        $this->layout = false;
        $user = new User();
        $fightOfferExercisesModel = new FightOfferExercise();

        $ownOffers = $user->getFightOffersByFirstUserId(Yii::$app->user->identity['id']);
        $exercises = $fightOfferExercisesModel->getFightOfferExercises();
        return $this->render('window-ownFightOffers', compact('ownOffers', 'exercises'));
    }

    public function actionDeleteOwnFightOffer($id)
    {
        $this->layout = false;
        $user = new User();
        $user->deleteFightOfferById($id);
        $ownOffers = $user->getFightOffersByFirstUserId(Yii::$app->user->identity['id']);
        return $this->render('window-ownFightOffers', compact('ownOffers'));
    }

    public function actionVerify($token)
    {
        $user = User::findOne(['token' => $token]);
        if (!empty($user)) {
            if ($user->token == $token)
                $user->verified = '1';
            $user->save();
            return $this->render('verification');
        } else {
            return $this->goHome();
        }
    }

    public function actionChangePassword($token)
    {
        $user = User::findOne(['token' => $token]);
        $changingPasswordForm = new ChangingPasswordForm();

        if (isset($_POST['ChangingPasswordForm'])) {
            $changingPasswordForm->attributes = Yii::$app->request->post('ChangingPasswordForm');
            if ($changingPasswordForm->validate() && $changingPasswordForm->changePassword($user)) {
                return $this->goHome();
            }
        }

        if (!empty($user)) {
            return $this->render('changing-password', compact('changingPasswordForm'));
        } else {
            return $this->goHome();
        }
    }

    public function actionSendMailChangePassword()
    {
        $changingPasswordForm = new ChangingPasswordForm();
        $statusMail = $changingPasswordForm->sendMail();
        return $this->render('mail-changing-password', compact('statusMail'));
    }


    public function actionRole()
    {
//        $admin = Yii::$app->authManager->createRole('admin');
//        $admin->description = 'Администратор';
//        Yii::$app->authManager->add($admin);
//
//        $author = Yii::$app->authManager->createRole('author');
//        $author->description = 'Автор';
//        Yii::$app->authManager->add($author);
//
//        $competition_organizer = Yii::$app->authManager->createRole('competition_organizer');
//        $competition_organizer->description = 'Организатор соревнований';
//        Yii::$app->authManager->add($competition_organizer);
//
//        $user = Yii::$app->authManager->createRole('user');
//        $user->description = 'Пользователь';
//        Yii::$app->authManager->add($user);
//
//        $blocked_user = Yii::$app->authManager->createRole('blocked_user');
//        $blocked_user->description = 'Заблокированный пользователь';
//        Yii::$app->authManager->add($blocked_user);
//
//        $moderator = Yii::$app->authManager->createRole('moderator');
//        $moderator->description = 'Модератор';
//        Yii::$app->authManager->add($moderator);

        /*!!!*/
//        $permit = Yii::$app->authManager->createPermission('createCompetition');
//        $permit->description = 'Право на создание соревнования';
//        Yii::$app->authManager->add($permit);
//
//        $role_a = Yii::$app->authManager->getRole('competition_organizer');
//        $role_b = Yii::$app->authManager->getRole('moderator');
//        $role_c = Yii::$app->authManager->getRole('admin');
//        $permit_a = Yii::$app->authManager->getPermission('createCompetition');
//        Yii::$app->authManager->addChild($role_a, $permit_a);
//        Yii::$app->authManager->addChild($role_b, $permit_a);
//        Yii::$app->authManager->addChild($role_c, $permit_a);

//        $userRole = Yii::$app->authManager->getRole('admin');
//        Yii::$app->authManager->assign($userRole, 1);

//        $userRole = Yii::$app->authManager->getRole('moderator');
//        Yii::$app->authManager->assign($userRole, 2);

//        $permit = Yii::$app->authManager->createPermission('moderate');
//        $permit->description = 'Право на модерирование';
//        Yii::$app->authManager->add($permit);


//        $role_a = Yii::$app->authManager->getRole('moderator');
//        $role_b = Yii::$app->authManager->getRole('admin');
//        $permit_a = Yii::$app->authManager->getPermission('moderate');
//        Yii::$app->authManager->addChild($role_a, $permit_a);
//        Yii::$app->authManager->addChild($role_b, $permit_a);

//         $userRole = Yii::$app->authManager->getRole('competition_organizer');
//         Yii::$app->authManager->assign($userRole, 3);

        return 141;
    }
}
