<?php

namespace app\controllers;

use app\models\Challenge;
use app\models\ChallengeCreatingForm;
use app\models\ChallengeParticipantForm;
use app\models\ChallengeParticipation;
use Yii;
use yii\web\Controller;

class ChallengeController extends Controller
{
    public function actionIndex()
    {
        $challengeModel = new Challenge();
        $challenges = $challengeModel->getChallenges();
        return $this->render('index', compact('challenges'));
    }

    public function actionCreating()
    {
        $creatingForm = new ChallengeCreatingForm();
        if (isset($_POST['ChallengeCreatingForm'])) {
            $creatingForm->attributes = Yii::$app->request->post('ChallengeCreatingForm');
            if ($creatingForm->validate() && $creatingForm->signUp()) {
                return $this->redirect('/challenges');
            }
        }
        return $this->render('creating', compact('creatingForm'));
    }

    public function actionView($id)
    {
        $challengeModel = new Challenge();
        $challengeParticipationModel = new ChallengeParticipation();

        $challenge = $challengeModel->getChallengeById($id);
        $challengeParticipations = $challengeParticipationModel->getChallengeById($id);
        return $this->render('view', compact('challenge', 'challengeParticipations'));
    }

    public function actionTakingPart($id)
    {
        $this->layout = false;
        $participantForm = new ChallengeParticipantForm();
        if (isset($_POST['ChallengeParticipantForm'])) {
            $participantForm->attributes = Yii::$app->request->post('ChallengeParticipantForm');
            if ($participantForm->validate() && $participantForm->takePart($id)) {
                $url = '/challenges/view?id=' . $id;
                return $this->redirect($url);
            }
        }
        return $this->render('taking-part', compact('participantForm'));
    }
}