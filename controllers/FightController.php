<?php

namespace app\controllers;

use app\models\Exercise;
use app\models\FightCreatingForm;
use Yii;
use yii\web\Controller;
use app\models\Fight;


class FightController extends Controller
{
    public function actionUpcomingFights()
    {
        $fightModel = new Fight();
        $fights = Fight::find()->asArray()->all();
        $participants = $fightModel->getParticipants();
        $exercises = $fightModel->getExercises();
        return $this->render('upcoming-fights', compact('fights', 'participants', 'exercises'));
    }

    public function actionCompletedFights()
    {
        $fightModel = new Fight();
        $fights = Fight::find()->asArray()->all();
        $participants = $fightModel->getParticipants();
        $exercises = $fightModel->getExercises();
        return $this->render('completed-fights', compact('fights', 'participants', 'exercises'));
    }

    public function actionCreating($id)
    {
        $this->layout = false;
        $creatingForm = new FightCreatingForm();

        $exercises = array();
        $exercisesQuery = Exercise::find()->asArray()->all();
        for ($i = 0; $i < count($exercisesQuery); $i++) {
            $exercises[$i + 1] = $exercisesQuery[$i]['name_exercise'];
        }

        if (isset($_POST['FightCreatingForm'])) {
            $creatingForm->attributes = Yii::$app->request->post('FightCreatingForm');
            if ($creatingForm->validate() && $creatingForm->signUp($id)) {
                $url = '/user/' . $id;
                return $this->redirect($url);
            }
        }
        return $this->renderAjax('creating', compact('creatingForm', 'id', 'exercises'));
    }
}