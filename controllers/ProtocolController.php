<?php

namespace app\controllers;

use Yii;
use app\models\UserCompetition;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProtocolController implements the CRUD actions for UserCompetition model.
 */
class ProtocolController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserCompetition models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => UserCompetition::find()->select('*')->asArray()
                ->innerJoin('user', '`user_competition`.`id_user`=`user`.`id`')
                ->where(['id_competition' => $id]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserCompetition model.
     * @param integer $id_user
     * @param integer $id_competition
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_user, $id_competition)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_user, $id_competition),
        ]);
    }

    /**
     * Updates an existing UserCompetition model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id_user
     * @param integer $id_competition
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_user, $id_competition)
    {
        $model = $this->findModel($id_user, $id_competition);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_user' => $model->id_user, 'id_competition' => $model->id_competition]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing UserCompetition model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id_user
     * @param integer $id_competition
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_user, $id_competition)
    {
        $this->findModel($id_user, $id_competition)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserCompetition model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id_user
     * @param integer $id_competition
     * @return UserCompetition the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_user, $id_competition)
    {
        if (($model = UserCompetition::findOne(['id_user' => $id_user, 'id_competition' => $id_competition])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
