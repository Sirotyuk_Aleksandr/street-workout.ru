<?php

namespace app\modules\moderation\models;

use Yii;

/**
 * This is the model class for table "sports_facility_comment".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_facility
 * @property string $content
 *
 * @property User $user
 * @property SportsFacility $facility
 */
class SportsFacilityComment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sports_facility_comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'id_facility', 'content'], 'required'],
            [['id_user', 'id_facility'], 'integer'],
            [['content'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_facility' => 'Id Facility',
            'content' => 'Content',
        ];
    }

}
