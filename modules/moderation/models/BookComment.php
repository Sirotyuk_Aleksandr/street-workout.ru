<?php

namespace app\modules\moderation\models;

use Yii;

/**
 * This is the model class for table "book_comment".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_book
 * @property string $content
 *
 */
class BookComment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'book_comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'id_book', 'content'], 'required'],
            [['id_user', 'id_book'], 'integer'],
            [['content'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_book' => 'Id Book',
            'content' => 'Content',
        ];
    }
}
