<?php

namespace app\modules\moderation\models;

use Yii;

/**
 * This is the model class for table "competition".
 *
 * @property int $id_competition
 * @property string $competition_name
 * @property string $city
 * @property int $id_creator
 * @property int $id_type_competition
 * @property string $date
 * @property double $contribution
 * @property int $age_from
 * @property int $age_to
 * @property string $description
 * @property string $is_checked
 *
 */
class Competition extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'competition';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['competition_name', 'city', 'id_creator', 'id_type_competition', 'date'], 'required'],
            [['id_creator', 'id_type_competition', 'age_from', 'age_to'], 'integer'],
            [['date'], 'safe'],
            [['contribution'], 'number'],
            [['description', 'is_checked'], 'string'],
            [['competition_name', 'city'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_competition' => 'Id Competition',
            'competition_name' => 'Competition Name',
            'city' => 'City',
            'id_creator' => 'Id Creator',
            'id_type_competition' => 'Id Type Competition',
            'date' => 'Date',
            'contribution' => 'Contribution',
            'age_from' => 'Age From',
            'age_to' => 'Age To',
            'description' => 'Description',
            'is_checked' => 'Статус проверки',
        ];
    }
}
