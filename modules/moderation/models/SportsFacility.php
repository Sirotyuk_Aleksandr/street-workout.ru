<?php

namespace app\modules\moderation\models;

use Yii;

/**
 * This is the model class for table "sports_facility".
 *
 * @property int $id_facility
 * @property string $name_facility
 * @property string $address
 * @property int $id_author
 * @property double $latitude
 * @property double $longitude
 * @property string $main_image
 * @property string $description
 * @property string $is_checked
 *
 * @property MarkSportsFacility[] $markSportsFacilities
 * @property User $author
 * @property SportsFacilityComment[] $sportsFacilityComments
 * @property UserFacility[] $userFacilities
 */
class SportsFacility extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sports_facility';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_facility', 'address', 'id_author', 'latitude', 'longitude'], 'required'],
            [['id_author'], 'integer'],
            [['latitude', 'longitude'], 'number'],
            [['description', 'is_checked'], 'string'],
            [['name_facility', 'address', 'main_image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_facility' => 'Id Facility',
            'name_facility' => 'Name Facility',
            'address' => 'Address',
            'id_author' => 'Id Author',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'main_image' => 'Main Image',
            'description' => 'Description',
            'is_checked' => 'Статус проверки',
        ];
    }
}
