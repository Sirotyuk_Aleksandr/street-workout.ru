<?php

namespace app\modules\moderation\models;

use Yii;

/**
 * This is the model class for table "challenge".
 *
 * @property int $id_challenge
 * @property string $name_challenge
 * @property string $date_creation
 * @property int $id_creator
 * @property string $video_reference
 * @property string $description
 * @property string $is_checked
 */
class Challenge extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'challenge';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_challenge', 'date_creation', 'id_creator', 'video_reference', 'description'], 'required'],
            [['date_creation'], 'safe'],
            [['id_creator'], 'integer'],
            [['description', 'is_checked'], 'string'],
            [['name_challenge', 'video_reference'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_challenge' => 'Id Challenge',
            'name_challenge' => 'Name Challenge',
            'date_creation' => 'Date Creation',
            'id_creator' => 'Id Creator',
            'video_reference' => 'Video Reference',
            'description' => 'Description',
            'is_checked' => 'Статус проверки',
        ];
    }
}
