<?php

namespace app\modules\moderation\models;

use Yii;

/**
 * This is the model class for table "article".
 *
 * @property int $id_article
 * @property int $id_author
 * @property int $id_category
 * @property string $name_article
 * @property string $date_publication
 * @property string $annotation
 * @property string $content
 * @property string $main_image
 * @property string $video_link
 * @property string $is_checked
 * @property string $is_advertising
 *
 * @property User $author
 * @property Category $category
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_author', 'id_category', 'name_article', 'date_publication', 'annotation'], 'required'],
            [['id_author', 'id_category'], 'integer'],
            [['date_publication'], 'safe'],
            [['annotation', 'content', 'is_checked', 'is_advertising'], 'string'],
            [['name_article', 'main_image', 'video_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_article' => 'Id Article',
            'id_author' => 'Id Author',
            'id_category' => 'Id Category',
            'name_article' => 'Name Article',
            'date_publication' => 'Date Publication',
            'annotation' => 'Annotation',
            'content' => 'Content',
            'main_image' => 'Main Image',
            'video_link' => 'Video Link',
            'is_checked' => 'Статус проверки',
            'is_advertising' => 'Is Advertising',
        ];
    }

}
