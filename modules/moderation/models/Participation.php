<?php

namespace app\modules\moderation\models;

use Yii;

/**
 * This is the model class for table "challenge_participation".
 *
 * @property int $id
 * @property int $id_challenge
 * @property int $id_participant
 * @property string $date_participation
 * @property string $video_reference
 * @property string $is_checked_participation
 *
 */
class Participation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'challenge_participation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_challenge', 'id_participant', 'date_participation', 'video_reference'], 'required'],
            [['id_challenge', 'id_participant'], 'integer'],
            [['date_participation'], 'safe'],
            [['is_checked_participation'], 'string'],
            [['video_reference'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_challenge' => 'Id Challenge',
            'id_participant' => 'Id Participant',
            'date_participation' => 'Date Participation',
            'video_reference' => 'Video Reference',
            'is_checked_participation' => 'Is Checked',
        ];
    }
}
