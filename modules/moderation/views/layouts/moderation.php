<?php
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <?= Html::csrfMetaTags() ?>
        <title>Модерация | <?= Html::encode($this->title) ?></title>
        <script src="http://api-maps.yandex.ru/2.0/?load=package.full&apikey=f1db1d62-8e17-4848-b69f-2fbae8ffd06f&lang=ru-RU"
                type="text/javascript"></script>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>

    <!-- Header with the top menu -->
    <header>
        <nav class="top-menu">
            <div class="container-fluid">
                <ul class="top-menu">
                    <li><?= Html::img('@web/images/logo.jpg', ['alt' => 'logo_workout', 'width' => '30%']); ?></li>
                    <li><a href="<?= Url::to(['/moderation/default'])  ?>">Главная</a></li>
                    <li class="dropdown-standart"><a href="" class="dropdown">Модерация</a>
                        <ul class="submenu-standart">
                            <li><a href="<?= Url::to(['/moderation/articles']) ?>">Статьи</a></li>
                            <li><a href="<?= Url::to(['/moderation/map']) ?>">Карта площадок</a></li>
                            <li><a href="<?= Url::to(['/moderation/challenges']) ?>">Workout - челленджи</a></li>
                            <li><a href="<?= Url::to(['/moderation/challenge-participation']) ?>">
                                    Участие в челленджах</a></li>
                            <li><a href="<?= Url::to(['/moderation/competitions']) ?>">Соревнования</a></li>
                            <li><a href="<?= Url::to(['/moderation/book-comments']) ?>">Отзывы о книгах</a></li>
                            <li><a href="<?= Url::to(['/moderation/facility-comments']) ?>">Отзывы о спортивных объектах</a></li>
                        </ul>
                    </li>
                    <li><a href="<?= Url::home();  ?>">Выйти из раздела модерации</a></li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- End of header with the top menu -->
    <div class="main_content">
        <?= $content; ?>
    </div>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>