<?php
$this->title = 'Главная';

use yii\helpers\Url;
?>
<div class="container-fluid">
    <h1>Раздел модерации</h1>
    <ul>
        <li><a href="<?= Url::to(['/moderation/articles']) ?>">Проверка статей</a></li>
        <li><a href="<?= Url::to(['/moderation/map']) ?>">Проверка меток на карте площадок</a></li>
        <li><a href="<?= Url::to(['/moderation/challenges']) ?>">Проверка Workout - челенджей</a></li>
        <li><a href="<?= Url::to(['/moderation/challenge-participation']) ?>">
                Проверка участников Workout - челленджей</a></li>
        <li><a href="<?= Url::to(['/moderation/competitions']) ?>">Проверка Workout - соревнований</a></li>
        <li><a href="<?= Url::to(['/moderation/book-comments']) ?>">Проверка отзывов о книгах</a></li>
        <li><a href="<?= Url::to(['/moderation/facility-comments']) ?>">Проверка отзывов о спортивных объектах</a></li>
    </ul>
</div>