<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sports Facilities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sports-facility-index">

    <h1>Проверка спортивных объектов на карте</h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_facility',
            'name_facility',
            'address',
            'login',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
