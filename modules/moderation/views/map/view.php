<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\moderation\models\SportsFacility */

$this->title = $model->id_facility;
$this->params['breadcrumbs'][] = ['label' => 'Sports Facilities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="sports-facility-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_facility], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_facility], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_facility',
            'name_facility',
            'address',
            'id_author',
            'latitude',
            'longitude',
            [
                'attribute'=>'main_image',
                'value'=> "@web/images/uploads/sports_objects/{$model->main_image}",
                'format' => ['image',['width'=>'210','height'=>'170']],
            ],
            'description:ntext',
            'is_checked',
        ],
    ]) ?>

</div>
