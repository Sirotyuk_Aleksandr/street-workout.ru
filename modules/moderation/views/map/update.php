<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\moderation\models\SportsFacility */

$this->title = 'Проверить объект: ' . $model->id_facility;
$this->params['breadcrumbs'][] = ['label' => 'Sports Facilities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_facility, 'url' => ['view', 'id' => $model->id_facility]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sports-facility-update">
    <div class="container-fluid">
        <div class="col-md-5 col-md-offset-4">
            <h1><?= Html::encode($this->title) ?></h1>

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
