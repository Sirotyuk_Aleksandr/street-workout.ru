<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\moderation\models\SportsFacility */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sports-facility-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'is_checked')->dropDownList([ '0', '1', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
