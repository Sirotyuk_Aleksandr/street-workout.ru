<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\moderation\models\Participation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="participation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'is_checked_participation')->dropDownList([ '0', '1', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
