<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Participations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participation-index">

    <h1>Проверка участников челленджей</h1>

    <p>
        <?= Html::a('Create Participation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name_challenge',
            'login',
            'date_participation',
            'video_reference',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
