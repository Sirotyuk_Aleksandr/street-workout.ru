<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\moderation\models\Competition */

$this->title = 'Проверить соревнование: ' . $model->id_competition;
$this->params['breadcrumbs'][] = ['label' => 'Competitions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_competition, 'url' => ['view', 'id' => $model->id_competition]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="competition-update">
    <div class="container-fluid">
        <div class="col-md-5 col-md-offset-4">
            <h1><?= Html::encode($this->title) ?></h1>

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
