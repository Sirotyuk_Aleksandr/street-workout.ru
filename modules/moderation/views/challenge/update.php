<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\moderation\models\Challenge */

$this->title = 'Проверить челлендж: ' . $model->id_challenge;
$this->params['breadcrumbs'][] = ['label' => 'Challenges', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_challenge, 'url' => ['view', 'id' => $model->id_challenge]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="challenge-update">
    <div class="container-fluid">
        <div class="col-md-5 col-md-offset-4">
            <h1><?= Html::encode($this->title) ?></h1>

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

</div>
