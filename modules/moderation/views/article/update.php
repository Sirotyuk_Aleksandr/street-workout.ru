<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\moderation\models\Article */

$this->title = 'Проверить статью: ' . $model->id_article;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_article, 'url' => ['view', 'id' => $model->id_article]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="article-update">
    <div class="container-fluid">
        <div class="col-md-5 col-md-offset-4">
            <h1><?= Html::encode($this->title) ?></h1>

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
