<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\moderation\models\Article */

$this->title = $model->id_article;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="article-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_article], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_article], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_article',
            'id_author',
            'id_category',
            'name_article',
            'date_publication',
            'annotation:ntext',
            'content:ntext',
            [
                'attribute'=>'main_image',
                'value'=> "@web/images/uploads/articles/{$model->main_image}",
                'format' => ['image',['width'=>'210','height'=>'170']],
            ],
            'video_link',
            'is_checked',
            'is_advertising',
        ],
    ]) ?>

</div>
