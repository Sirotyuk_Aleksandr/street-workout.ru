<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Articles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">

    <h1>Проверка статей</h1>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_article',
            'login',
            'name_category',
            'name_article',
            'date_publication',
            //'is_checked',
            //'annotation:ntext',
            //'content:ntext',
            //'main_image',
            //'is_advertising',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>