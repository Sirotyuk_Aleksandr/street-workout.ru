<?php

namespace app\modules\moderation\controllers;

class DefaultController extends AppModerationController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
