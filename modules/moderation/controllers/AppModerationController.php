<?php

namespace app\modules\moderation\controllers;

use Exception;
use yii\filters\AccessControl;
use yii\web\Controller;

class AppModerationController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['moderator'],
                    ],
                ],

            ],
        ];
    }
}