<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru',
    'modules' => [
        'moderation' => [
            'class' => 'app\modules\moderation\Module',
            'layout' => 'moderation',
        ],
    ],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => 'cache',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'rivsKWxA7eOgM8dsh7EqwudJYJKysJD0',
            'baseUrl' => ''
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'appendTimestamp' => true,
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.mail.ru',
                'username' => 'ninja_241296@mail.ru',
                'password' => 'j[etyysqgfhjkm',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'registration' => 'site/registration',
                'rules' => 'site/rules',
                'authorization' => 'site/login',
                'logout' => 'site/logout',
                'profile' => 'site/profile',
                'profile/edit' => 'site/edit-profile',
                'books' => 'book/index',
                'elements' => 'element/index',
                'elements/1lvl' => 'element/1lvl',
                'elements/2lvl' => 'element/2lvl',
                'elements/3lvl' => 'element/3lvl',
                'articles' => 'article/index',
                'articles/healthy-food' => 'article/index-food',
                'competitions' => 'competition/index',
                'competitions/creating' => 'competition/creating',
                'user/<id:\d+>' =>'site/view-user',
                'fights/upcoming-fights' => 'fight/upcoming-fights',
                'fights/completed-fights' => 'fight/completed-fights',
                'competitions/completed' => 'competition/completed',
                'competition/<id:\d+>' =>'competition/view',
                'competition/<id:\d+>/participants' =>'competition/view-participants',
                'challenges' => 'challenge/index',
                'challenges/creating' => 'challenge/creating',
                'challenges/view' => 'challenge/view',
                'challenges/taking-part' => 'challenge/taking-part',
                'moderation/articles' => 'moderation/article',
                'moderation/challenges' => 'moderation/challenge',
                'moderation/competitions' => 'moderation/competition',
                'moderation/challenge-participation' => 'moderation/participation',
                'moderation/book-comments' => 'moderation/book',
                'moderation/facility-comments' => 'moderation/facility',
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        //uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
