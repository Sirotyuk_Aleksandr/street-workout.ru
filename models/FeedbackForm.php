<?php

namespace app\models;

use Yii;
use yii\base\Model;

class FeedbackForm extends Model
{
    public $name;
    public $email;
    public $content;

    public function attributeLabels()
    {
        return [
            'name' => 'Ваше имя:',
            'email' => 'Ваш E-Mail:',
            'content' => 'Отзыв:',
        ];
    }

    public function rules()
    {
        return [
            [['name', 'name', 'content'], 'required'],
            ['name', 'string', 'min' => 4],
            ['content', 'string', 'min' => 4],
            ['email', 'email']
        ];
    }

    public function sendComment(){
        Yii::$app->mailer->compose()
            ->setFrom(['ninja_241296@mail.ru' => $this->email])
            ->setTo(Yii::$app->params['adminEmail'])
            ->setSubject('Feedback')
            ->setHtmlBody('<h3>Имя отправителя: '.$this->name.'.</h3><br/>'.$this->content)
            ->send();
    }
}