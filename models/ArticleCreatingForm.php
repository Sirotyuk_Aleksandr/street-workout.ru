<?php

namespace app\models;

use Yii;
use yii\base\Model;

class ArticleCreatingForm extends Model
{
    public $name_article;
    public $annotation;
    public $content;
    public $main_image;
    public $is_advertising;

    public function attributeLabels()
    {
        return [
            'name_article' => 'Название статьи:',
            'annotation' => 'Аннотация',
            'content' => 'Контент:',
            'main_image' => 'Главная картинка:',
            'is_advertising' => 'Рекламная статья',
        ];
    }

    public function rules()
    {
        return [
            [['name_article', 'content', 'annotation'], 'required'],
            ['content', 'string', 'min' => 10, 'max' => 6000],
            ['name_article', 'string', 'min' => 4, 'max' => 30],
            ['annotation', 'string', 'min' => 4, 'max' => 60],
            ['main_image', 'file', 'extensions' => ['png', 'jpg']],
            [['is_advertising'], 'safe'],
        ];
    }

    public function createArticle($categoryId)
    {
        $article = new Article();
        $article->name_article = $this->name_article;
        $article->annotation = $this->annotation;
        $article->content = $this->content;
        $article->id_author = Yii::$app->user->identity['id'];
        $article->id_category = $categoryId;
        $article->main_image = $this->main_image;
        $article->is_advertising = $this->is_advertising;
        return $article->save();
    }
}