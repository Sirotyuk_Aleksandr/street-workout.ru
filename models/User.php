<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use Yii;

class User extends ActiveRecord implements IdentityInterface
{
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {

    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {

    }

    public function validateAuthKey($authKey)
    {

    }

    /**
     * @param $id
     * @return string
     */
    public function getBooksByUserId($id)
    {
        $books = Yii::$app->db->createCommand('SELECT `book`.`name`, `book`.`image`, `book`.`id` FROM `user` 
            JOIN `user_book` ON `user`.`id` = `user_book`.`id_user`
            JOIN `book` ON `user_book`.`id_book`= `book`.`id`
            WHERE `user`.`id` = :id', [':id' => $id])->queryAll();
        return $books;
    }

    public function deleteBook($id)
    {
        Yii::$app->db->createCommand('DELETE FROM `user_book`
        WHERE `user_book`.`id_user` = :id_user AND `user_book`.`id_book` = :id_book',
            [':id_book' => $id, ':id_user' => Yii::$app->user->identity['id']])->query();
        return true;
    }

    public function getElementsByUserId($id)
    {
        $elements = Yii::$app->db->createCommand('SELECT `element`.`name`,
                `element`.`image`, `element`.`id`, `element`.`level` 
            FROM `user` 
            JOIN `user_element` ON `user`.`id` = `user_element`.`id_user`
            JOIN `element` ON `user_element`.`id_element`= `element`.`id`
            WHERE `user`.`id` = :id', [':id' => $id])->queryAll();
        return $elements;
    }

    public function deleteElement($id)
    {
        Yii::$app->db->createCommand('DELETE FROM `user_element`
        WHERE `user_element`.`id_user` = :id_user AND `user_element`.`id_element` = :id_element',
            [':id_element' => $id, ':id_user' => Yii::$app->user->identity['id']])->query();
        return true;
    }

    public function getFightOffersBySecondUserId($id)
    {
        $offers = Yii::$app->db->createCommand('SELECT *
            FROM `fight_offer` 
            JOIN `user` ON `fight_offer`.`id_first_user` = `user`.`id`
            WHERE `fight_offer`.`id_second_user` = :id',
            [':id' => $id])->queryAll();
        return $offers;
    }

    public function deleteFightOfferById($id_offer)
    {
        Yii::$app->db->createCommand('DELETE FROM `fight_offer_exercise`
        WHERE `fight_offer_exercise`.`id_fight_offer` = :id',
            [':id' => $id_offer])->query();
        Yii::$app->db->createCommand('DELETE FROM `fight_offer`
        WHERE `fight_offer`.`id_offer` = :id',
            [':id' => $id_offer])->query();
        return true;
    }

    public function getFightOffersByFirstUserId($id)
    {
        $ownOffers = Yii::$app->db->createCommand('SELECT *
            FROM `fight_offer` 
            JOIN `user` ON `fight_offer`.`id_second_user` = `user`.`id`
            WHERE `fight_offer`.`id_first_user` = :id',
            [':id' => $id])->queryAll();
        return $ownOffers;
    }

    public function acceptOfferById($id_offer)
    {
        $offer = FightOffer::findOne($id_offer);
        $fightModel = new Fight();
        $fightModel->city = $offer->city;
        $fightModel->playground_address = $offer->playground_address;
        $fightModel->date_fight = $offer->date_fight;
        $fightModel->save();

        $fightOfferExercises = FightOfferExercise::find()->where(['id_fight_offer' => $id_offer])
            ->asArray()->all();
        $lastFight = Fight::find()->orderBy(['id' => SORT_DESC])->one();
        for ($i = 0; $i < count($fightOfferExercises); $i++) {
            $exerciseFightModel = new ExerciseFight();
            $exerciseFightModel->id_fight = $lastFight['id'];
            $exerciseFightModel->id_exercise = $fightOfferExercises[$i]['id_exercise'];
            if ($i != 2) {
                $exerciseFightModel->bonus_exercise = 0;
            } else {
                $exerciseFightModel->bonus_exercise = 1;
            }
            $exerciseFightModel->save();
        }

        $userFightModel = new UserFight();
        $userFightModel->id_fight = $lastFight['id'];
        $userFightModel->id_user = $offer->id_first_user;
        $userFightModel->save();

        $userFightModel = new UserFight();
        $userFightModel->id_fight = $lastFight['id'];
        $userFightModel->id_user = $offer->id_second_user;
        $userFightModel->save();

        Yii::$app->db->createCommand('DELETE FROM `fight_offer_exercise`
        WHERE `fight_offer_exercise`.`id_fight_offer` = :id',
            [':id' => $id_offer])->query();
        Yii::$app->db->createCommand('DELETE FROM `fight_offer`
        WHERE `fight_offer`.`id_offer` = :id',
            [':id' => $id_offer])->query();
        return true;
    }
}
