<?php

namespace app\models;

use yii\db\ActiveRecord;

class SportsFacilityComment extends ActiveRecord
{
    public static function tableName()
    {
        return 'sports_facility_comment';
    }
}