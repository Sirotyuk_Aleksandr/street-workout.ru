<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

class Competition extends ActiveRecord
{
    public function getCompetitionById($id){
        $competition = Yii::$app->db->createCommand('SELECT * FROM `competition` 
            JOIN `type_competition` ON `competition`.`id_type_competition` = `type_competition`.`id`
            JOIN `user` ON `competition`.`id_creator`= `user`.`id`
            WHERE `competition`.`id_competition` = :id_competition',
            [':id_competition' => $id])->queryAll();
        return $competition;
    }
    public function getBrieflyCompetitions(){
        $competitions = Yii::$app->db->createCommand('SELECT * FROM `competition` 
            JOIN `type_competition` ON `competition`.`id_type_competition` = `type_competition`.`id`
            JOIN `user` ON `competition`.`id_creator`= `user`.`id`
            WHERE `competition`.`is_checked` = \'1\'')->queryAll();
        return $competitions;
    }
    public function getAwardsById($id){
        $awards = Yii::$app->db->createCommand('SELECT `award`.`name` FROM `competition_award` 
            JOIN `competition` ON `competition_award`.`id_competition` = `competition`.`id_competition`
            JOIN `award` ON `competition_award`.`id_award`= `award`.`id`
            WHERE `competition`.`id_competition` = :id_competition',
            [':id_competition' => $id])->queryAll();
        return $awards;
    }
    public function getSponsorsById($id){
        $sponsors = Yii::$app->db->createCommand('SELECT `sponsor`.`name` FROM `competition_sponsor` 
            JOIN `competition` ON `competition_sponsor`.`id_competition` = `competition`.`id_competition`
            JOIN `sponsor` ON `competition_sponsor`.`id_sponsor`= `sponsor`.`id`
            WHERE `competition`.`id_competition` = :id_competition',
            [':id_competition' => $id])->queryAll();
        return $sponsors;
    }
    public function addParticipant($id_competition){
        $competition = Yii::$app->db->createCommand('SELECT * FROM `user_competition` 
            WHERE `user_competition`.`id_user` = :id_user AND `user_competition`.`id_competition` = :id_competition',
            [':id_competition' => $id_competition, ':id_user' => Yii::$app->user->identity['id']])->queryAll();
        if(!$competition) {
            Yii::$app->db->createCommand('INSERT INTO `user_competition` (`id_user`, `id_competition`)
                VALUES (:id_user,:id_competition)',
                [':id_competition' => $id_competition, ':id_user' => Yii::$app->user->identity['id']])->query();
        }
        return true;
    }
    public function getParticipantsById($id_competition){
        $participants =  Yii::$app->db->createCommand('SELECT * FROM `user_competition` 
            JOIN `user` ON `user_competition`.`id_user` = `user`.`id`
            WHERE `user_competition`.`id_competition` = :id_competition',
            [':id_competition' => $id_competition])->queryAll();
        return $participants;
    }
}