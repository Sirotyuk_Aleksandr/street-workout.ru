<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

class Challenge extends ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date_creation'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getChallenges(){
        $challenges = Yii::$app->db->createCommand('SELECT * FROM `challenge` 
            JOIN `user` ON `challenge`.`id_creator` = `user`.`id`
            WHERE `challenge`.`is_checked` = \'1\'
            ORDER BY `challenge`.`date_creation`')->queryAll();
        return $challenges;
    }

    public function getChallengeById($id){
        $challenge = Yii::$app->db->createCommand('SELECT * FROM `challenge` 
            JOIN `user` ON `challenge`.`id_creator` = `user`.`id`
            WHERE `challenge`.`id_challenge` = :id_challenge',
            [':id_challenge' => $id])->queryAll();
        return $challenge;
    }
}