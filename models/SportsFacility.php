<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class SportsFacility extends ActiveRecord
{
    public static function tableName()
    {
        return 'sports_facility';
    }

    public function getSportsObjectById($id)
    {
        $sportsObject = Yii::$app->db->createCommand('SELECT * FROM `sports_facility` 
            JOIN `user` ON `sports_facility`.`id_author` = `user`.`id`
            WHERE `sports_facility`.`id_facility` = :id_facility',
            [':id_facility' => $id])->queryAll();
        return $sportsObject;
    }

    public function getSportsObjects()
    {
        $sportsObjects = Yii::$app->db->createCommand('SELECT * FROM `sports_facility` 
            JOIN `user` ON `sports_facility`.`id_author` = `user`.`id`
            WHERE `sports_facility`.`is_checked` = \'1\'')->queryAll();
        return $sportsObjects;
    }

    public function getFacilityComment($id)
    {
        $commentFacility = Yii::$app->db->createCommand('SELECT `content`,`id_user`, `login`  
            FROM `sports_facility_comment` 
            JOIN `user` ON `sports_facility_comment`.`id_user` = `user`.`id`
            WHERE `sports_facility_comment`.`id_facility` = :id_facility',
            [':id_facility' => $id])->queryAll();
        return $commentFacility;
    }

}