<?php
/**
 * Created by PhpStorm.
 * User: Aleks
 * Date: 05.10.2018
 * Time: 7:33
 */

namespace app\models;

use yii\base\Model;
use app\models\User;
use Yii;


class EditProfileForm extends Model
{
    public $image_avatar;
    public $last_name;
    public $name;
    public $patronymic;
    public $date_birth;
    public $experience;
    public $growth;
    public $weight;
    public $max_pulling_up;
    public $max_push_up_bars;
    public $max_push_up;
    public $max_bench_press;
    public $reference_youtube;
    public $reference_vk;

    public function attributeLabels()
    {
        return [
            'image_avatar' => 'Аватар',
            'last_name' => 'Фамилия:',
            'name' => 'Имя:',
            'patronymic' => 'Отчество:',
            'date_birth' => 'Дата рождения:',
            'experience' => 'Стаж:',
            'growth' => 'Рост:',
            'weight' => 'Вес:',
            'max_pulling_up' => 'Максимальное количество подтягиваний:',
            'max_push_up_bars' => 'Максимальное количество отжиманий на брусьях:',
            'max_push_up' => 'Максимальное количество отжиманий от пола:',
            'max_bench_press' => 'Максимальный вес штанги при жиме лежа:',
            'reference_youtube' => 'Ссылка на Youtube-аккаунт:',
            'reference_vk' => 'Ссылка на VKontakte-аккаунт:'
        ];
    }

    public function rules()
    {
        return [
            ['last_name', 'string', 'max' => 30],
            ['name', 'string', 'max' => 30],
            ['patronymic', 'string', 'max' => 30],
            ['reference_youtube', 'string', 'max' => 200],
            ['reference_vk', 'string', 'max' => 200],
            ['image_avatar', 'file', 'extensions' => ['png', 'jpg']],
            ['experience', 'integer', 'max' => 100],
            ['max_pulling_up', 'integer', 'max' => 500],
            ['max_push_up_bars', 'integer', 'max' => 500],
            ['max_push_up', 'integer', 'max' => 500],
            ['max_bench_press', 'integer', 'max' => 500],
            ['growth', 'integer', 'max' => 200],
            ['weight', 'integer', 'max' => 200],
        ];
    }

    public function edit(){
        $user = User::findOne(Yii::$app->user->identity['id']);
        if($this->image_avatar != null) {
            $user->image_avatar = $this->image_avatar;
        }
        if($this->last_name != null) {
            $user->last_name = $this->last_name;
        }
        if($this->name != null) {
            $user->name = $this->name;
        }
        if($this->patronymic != null) {
            $user->patronymic = $this->patronymic;
        }
        if($this->date_birth != null) {
            $user->date_birth = $this->date_birth;
        }
        if($this->experience != null) {
            $user->experience = $this->experience;
        }
        if($this->growth != null) {
            $user->growth = $this->growth;
        }
        if($this->weight != null) {
            $user->weight = $this->weight;
        }
        if($this->max_bench_press != null) {
            $user->max_bench_press = $this->max_bench_press;
        }
        if($this->max_pulling_up != null) {
            $user->max_pulling_up = $this->max_pulling_up;
        }
        if($this->max_push_up != null) {
            $user->max_push_up = $this->max_push_up;
        }
        if($this->max_push_up_bars != null) {
            $user->max_push_up_bars = $this->max_push_up_bars;
        }
        if($this->reference_vk != null) {
            $user->reference_vk = $this->reference_vk;
        }
        if($this->reference_youtube != null) {
            $user->reference_youtube = $this->reference_youtube;
        }
        return $user->save();
    }
}