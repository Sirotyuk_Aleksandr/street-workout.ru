<?php

namespace app\models;

use yii\db\ActiveRecord;

class FightOffer extends ActiveRecord
{
    public static function tableName()
    {
        return 'fight_offer';
    }
}