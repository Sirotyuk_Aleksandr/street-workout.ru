<?php
/**
 * Created by PhpStorm.
 * User: Aleks
 * Date: 05.10.2018
 * Time: 7:33
 */

namespace app\models;

use yii\base\Model;
use app\models\User;
use Yii;

class RegistrationForm extends Model
{
    public $login;
    public $email;
    public $password;
    public $repeatPassword;
    public $acceptingRules;

    public function attributeLabels()
    {
        return [
            'login' => 'Логин:',
            'email' => 'E-mail:',
            'password' => 'Пароль:',
            'repeatPassword' => 'Повторный пароль:',
            'acceptingRules' => 'Принять правила сообщества',
        ];
    }

    public function rules()
    {
        return [
            [['login', 'email', 'password', 'repeatPassword'], 'required'],
            ['login', 'string', 'min' => 4],
            ['login', 'unique', 'targetClass'=>'app\models\User'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass'=>'app\models\User'],
            ['password', 'string', 'min' => 5],
            ['repeatPassword', 'compare', 'compareAttribute'=>'password', 'message'=>"Пароли не совпадают"],
            ['acceptingRules', 'compare', 'compareValue' => 1, 'message' => 'Необходимо принять правила сообщества!']
        ];
    }

    public function signUp($token){
        $user = new User();
        $user->login = $this->login;
        $user->email = $this->email;
        $user->password = Yii::$app->security->generatePasswordHash($this->password);
        $user->token = $token;
        return $user->save();
    }

    public function verifyUser($token){
        $url = 'street-workout.ru/site/verify?token='.$token;
        $login = $this->login;
        Yii::$app->mailer->compose('verification', compact('url', 'login'))
            ->setFrom(['ninja_241296@mail.ru'=>'Street Workout и саморазвитие'])
            ->setTo($this->email)
            ->setSubject('Verification')
            ->send();
        return true;
    }
}