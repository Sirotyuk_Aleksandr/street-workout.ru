<?php

namespace app\models;

use yii\base\Model;
use Yii;

class FightCreatingForm extends Model
{
    public $id_first_user;
    public $id_second_user;
    public $city;
    public $date_fight;
    public $id_exercise1;
    public $id_exercise2;
    public $id_exercise3;
    public $playground_address;


    public function attributeLabels()
    {
        return [
            'city' => 'Город:',
            'date_fight' => 'Дата зарубы:',
            'id_exercise1' => 'Первое упражнение:',
            'id_exercise2' => 'Второе упражнение:',
            'id_exercise3' => 'Третье упражнение:',
            'playground_address' => 'Адрес площадки'
        ];
    }

    public function rules()
    {
        return [
            [['city', 'date_fight', 'playground_address',
                'id_exercise1','id_exercise2','id_exercise3'], 'required'],
            ['city', 'string', 'min' => 3, 'max' => 70],
            ['playground_address', 'string', 'min' => 5, 'max' => 100],
        ];
    }

    public function signUp($id){
        $isSave = true;
        $fight_offer = new FightOffer();
        $fight_offer->id_first_user = Yii::$app->user->identity['id'];
        $fight_offer->id_second_user = $id;
        $fight_offer->city = $this->city;
        $fight_offer->date_fight = $this->date_fight;
        $fight_offer->playground_address = $this->playground_address;
        if(!$fight_offer->save()){
            $isSave = false;
        }

        $lastIdFightOffer = FightOffer::find()->select(['id_offer'])->orderBy(['id_offer' => SORT_DESC])->one();

        $fight_offer_exercise = new FightOfferExercise();
        $fight_offer_exercise->id_fight_offer = $lastIdFightOffer['id_offer'];
        $fight_offer_exercise->id_exercise = $this->id_exercise1;
        if(!$fight_offer_exercise->save()){
            $isSave = false;
        }

        $fight_offer_exercise = new FightOfferExercise();
        $fight_offer_exercise->id_fight_offer = $lastIdFightOffer['id_offer'];
        $fight_offer_exercise->id_exercise = $this->id_exercise2;
        if(!$fight_offer_exercise->save()){
            $isSave = false;
        }

        $fight_offer_exercise = new FightOfferExercise();
        $fight_offer_exercise->id_fight_offer = $lastIdFightOffer['id_offer'];
        $fight_offer_exercise->id_exercise = $this->id_exercise3;
        $fight_offer_exercise->bonus_exercise = '1';
        if(!$fight_offer_exercise->save()){
            $isSave = false;
        }

        if($isSave) {
            return true;
        }
    }
}