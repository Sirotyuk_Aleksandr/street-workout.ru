<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

class ChallengeParticipation extends ActiveRecord
{
    public static function tableName()
    {
        return 'challenge_participation';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date_participation'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getChallengeById($id){
        $challengeParticipation = Yii::$app->db->createCommand('SELECT * FROM `challenge_participation` 
            JOIN `user` ON `challenge_participation`.`id_participant` = `user`.`id`
            WHERE `challenge_participation`.`id_challenge` = :id_challenge',
            [':id_challenge' => $id])->queryAll();
        return $challengeParticipation;
    }
}