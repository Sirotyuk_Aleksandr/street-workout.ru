<?php

namespace app\models;

use yii\db\ActiveRecord;

class ExerciseFight extends ActiveRecord
{
    public static function tableName()
    {
        return 'exercise_fight';
    }
}