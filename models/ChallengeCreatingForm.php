<?php

namespace app\models;

use Yii;
use yii\base\Model;

class ChallengeCreatingForm extends Model
{
    public $name_challenge;
    public $video_reference;
    public $description;

    public function attributeLabels()
    {
        return [
            'name_challenge' => 'Название челленджа:',
            'video_reference' => 'Ссылка на видео:',
            'description' => 'Описание челленджа:',
        ];
    }

    public function rules()
    {
        return [
            [['name_challenge', 'video_reference', 'description'], 'required'],
            ['name_challenge', 'string', 'min' => 4, 'max' => 50],
            ['video_reference', 'string', 'min' => 7, 'max' => 1000],
            ['description', 'string', 'min' => 10, 'max' => 2000],
        ];
    }

    public function signUp()
    {
        $challenge = new Challenge();
        $challenge->name_challenge = $this->name_challenge;
        $challenge->video_reference = $this->video_reference;
        $challenge->description = $this->description;
        $challenge->id_creator = Yii::$app->user->identity['id'];
        return $challenge->save();
    }
}