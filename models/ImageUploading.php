<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class ImageUploading extends Model
{
    public function uploadImage(UploadedFile $file, $typeImage)
    {
        $filename = strtolower(md5(uniqid($file->baseName)) . '.' . $file->extension);

        if($typeImage == 'article') {
            $file->saveAs(Yii::getAlias('@web') . 'images/uploads/articles/' . $filename);
        }

        if($typeImage == 'editingProfile') {
            $file->saveAs(Yii::getAlias('@web') . 'images/uploads/profile/' . $filename);
        }

        if($typeImage == 'sportsFacility') {
            $file->saveAs(Yii::getAlias('@web') . 'images/uploads/sports_objects/' . $filename);
        }

        return $filename;
    }


}