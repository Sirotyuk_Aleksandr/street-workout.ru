<?php

namespace app\models;

use yii\db\ActiveRecord;

class UserFacility extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_facility';
    }
}