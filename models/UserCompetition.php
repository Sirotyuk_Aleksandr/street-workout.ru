<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_competition".
 *
 * @property int $id_user
 * @property int $id_competition
 * @property int $place
 * @property int $result
 *
 * @property User $user
 * @property Competition $competition
 */
class UserCompetition extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_competition';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'id_competition'], 'required'],
            [['id_user', 'id_competition', 'place', 'result'], 'integer'],
            [['id_user', 'id_competition'], 'unique', 'targetAttribute' => ['id_user', 'id_competition']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_user' => 'ИД пользователя',
            'id_competition' => 'ИД соревнований',
            'place' => 'Место',
            'result' => 'Результат',
        ];
    }
}
