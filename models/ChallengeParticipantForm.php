<?php

namespace app\models;

use Yii;
use yii\base\Model;

class ChallengeParticipantForm extends Model
{
    public $video_reference;

    public function attributeLabels()
    {
        return [
            'video_reference' => 'Ссылка на видео:',
        ];
    }

    public function rules()
    {
        return [
            [['video_reference'], 'required'],
            ['video_reference', 'string', 'min' => 7],
        ];
    }

    public function takePart($id)
    {
        $challengeParticipation = new ChallengeParticipation();
        $challengeParticipation->id_challenge = $id;
        $challengeParticipation->id_participant = Yii::$app->user->identity['id'];
        $challengeParticipation->video_reference = $this->video_reference;
        return $challengeParticipation->save();
    }
}