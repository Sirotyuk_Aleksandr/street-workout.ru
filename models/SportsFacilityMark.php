<?php

namespace app\models;

use yii\db\ActiveRecord;

class SportsFacilityMark extends ActiveRecord
{
    public static function tableName()
    {
        return 'mark_sports_facility';
    }
}