<?php

namespace app\models;

use Yii;
use yii\base\Model;

class SportsFacilityCreatingForm extends Model
{
    public $name_facility;
    public $address;
    public $latitude;
    public $longitude;
    public $main_image;
    public $description;

    public function attributeLabels()
    {
        return [
            'name_facility' => 'Название:',
            'address' => 'Адрес:',
            'latitude' => 'Широта:',
            'longitude' => 'Долгота:',
            'main_image' => 'Главная картинка:',
            'description' => 'Описание:'
        ];
    }

    public function rules()
    {
        return [
            [['name_facility', 'latitude', 'longitude', 'address'], 'required'],
            ['description', 'safe'],
            ['name_facility', 'string', 'min' => 4, 'max' => 50],
            ['address', 'string', 'min' => 4, 'max' => 50],
            ['description', 'string', 'min' => 4, 'max' => 200],
            ['main_image', 'file', 'extensions' => ['png', 'jpg']],
            ['latitude', 'double'],
            ['longitude', 'double'],
        ];
    }

    public function createFacility()
    {
        $facility = new SportsFacility();
        $facility->name_facility = $this->name_facility;
        $facility->address = $this->address;
        $facility->id_author = Yii::$app->user->identity['id'];
        $facility->latitude = $this->latitude;
        $facility->longitude = $this->longitude;
        $facility->main_image = $this->main_image;
        if ($this->description != null) {
            $facility->description = $this->description;
        }
        return $facility->save();
    }
}