<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class FightOfferExercise extends ActiveRecord
{
    public static function tableName()
    {
        return 'fight_offer_exercise';
    }
    public function getFightOfferExercises(){
        $exercises = Yii::$app->db->createCommand('SELECT * FROM `fight_offer_exercise` 
            JOIN `fight_offer` ON `fight_offer_exercise`.`id_fight_offer`= `fight_offer`.`id_offer`
            JOIN `exercise` ON `fight_offer_exercise`.`id_exercise`= `exercise`.`id`')->queryAll();
        return $exercises;
    }
}