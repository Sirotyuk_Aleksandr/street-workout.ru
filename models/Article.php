<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

class Article extends ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date_publication'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getArticles()
    {
        $articles = Yii::$app->db->createCommand('SELECT * FROM `article` 
            JOIN `user` ON `article`.`id_author` = `user`.`id`
            WHERE `article`.`id_category` = 2 AND `article`.`is_checked` = \'1\'
            ORDER BY `article`.`date_publication`')->queryAll();
        return $articles;
    }

    public function getArticleById($id)
    {
        $article = Yii::$app->db->createCommand('SELECT * FROM `article` 
            JOIN `user` ON `article`.`id_author` = `user`.`id`
            WHERE `article`.`id_article` = :id_article',
            [':id_article' => $id])->queryAll();
        return $article;
    }

    public function getFoodArticles()
    {
        $articles = Yii::$app->db->createCommand('SELECT * FROM `article` 
            JOIN `user` ON `article`.`id_author` = `user`.`id`
            WHERE `article`.`id_category` = 4 AND `article`.`is_checked` = \'1\'
            ORDER BY `article`.`date_publication`')->queryAll();
        return $articles;
    }
}