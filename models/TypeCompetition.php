<?php

namespace app\models;

use yii\db\ActiveRecord;

class TypeCompetition extends ActiveRecord
{
    public static function tableName()
    {
        return 'type_competition';
    }
}