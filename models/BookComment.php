<?php

namespace app\models;

use yii\db\ActiveRecord;

class BookComment extends ActiveRecord
{
    public static function tableName()
    {
        return 'book_comment';
    }
}