<?php

namespace app\models;

use Yii;
use yii\base\Model;

class ChangingPasswordForm extends Model
{
    public $login;
    public $password;
    public $newPassword;
    public $newRepeatPassword;

    public function attributeLabels()
    {
        return [
            'login' => 'Логин:',
            'password' => 'Текущий пароль:',
            'newPassword' => 'Новый пароль:',
            'newRepeatPassword' => 'Повторный новый пароль:',
        ];
    }

    public function rules()
    {
        return [
            [['login', 'password', 'newPassword', 'newRepeatPassword'], 'required'],
            ['newPassword', 'string', 'min' => 5],
            ['newRepeatPassword', 'compare', 'compareAttribute'=>'newPassword', 'message'=>"Пароли не совпадают"],
            ['password', 'validatePassword'],
        ];
    }

    public function validatePassword($attribute){
        $user = $this->getUser();
        if(!$user || !Yii::$app->security->validatePassword($this->password, $user->password)){
            $this->addError($attribute, 'Логин/пароль введены неверно');
        }
    }

    public function getUser(){
        return User::findOne(['login' => $this->login]);
    }

    public function changePassword($user){
        $user->password = Yii::$app->security->generatePasswordHash($this->newPassword);
        return $user->save();
    }

    public function sendMail(){
        $user = User::findOne(['id' => Yii::$app->user->identity['id']]);
        if(!empty($user['token'])) {
            $url = 'street-workout.ru/site/change-password?token=' . $user['token'];
            $login = $user['login'];
            Yii::$app->mailer->compose('changing-password', compact('url', 'login'))
                ->setFrom(['ninja_241296@mail.ru' => 'Street Workout и саморазвитие'])
                ->setTo($user['email'])
                ->setSubject('Changing Password')
                ->send();
            return true;
        }
       return false;
    }
}