<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

class Fight extends ActiveRecord
{
    public function getFights()
    {
        $fights = Yii::$app->db->createCommand('SELECT * FROM `fight` 
            JOIN `exercise` ON `fight`.`id` = `exercise`.`id_fight`
            JOIN `user_fight` ON `fight`.`id`= `user_fight`.`id_fight`
            JOIN `user` ON `user_fight`.`id_user`= `user`.`id`
            WHERE `user_fight`.`is_winner` IS NULL')->queryAll();
        return $fights;
    }

    public function getParticipants()
    {
        $participants = Yii::$app->db->createCommand('SELECT * FROM `user_fight` 
            JOIN `user` ON `user_fight`.`id_user`= `user`.`id`
            WHERE `user_fight`.`is_winner` IS NULL')->queryAll();
        return $participants;
    }

    public function getExercises()
    {
        $exercises = Yii::$app->db->createCommand('SELECT * FROM `exercise_fight` 
            JOIN `fight` ON `exercise_fight`.`id_fight`= `fight`.`id`
            JOIN `exercise` ON `exercise_fight`.`id_exercise`= `exercise`.`id`')->queryAll();
        return $exercises;
    }
}