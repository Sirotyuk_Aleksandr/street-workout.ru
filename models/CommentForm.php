<?php
/**
 * Created by PhpStorm.
 * User: Aleks
 * Date: 16.11.2018
 * Time: 12:09
 */

namespace app\models;

use yii\base\Model;
use Yii;

class CommentForm extends Model
{
    public $text;

    public function attributeLabels()
    {
        return [
            'text' => '',
        ];
    }
    public function rules()
    {
        return [
            ['text', 'required']
        ];
    }
    public function writeBookComment($id){
        $bookCommentModel = new BookComment();
        $bookCommentModel->id_book = $id;
        $bookCommentModel->id_user = Yii::$app->user->identity['id'];
        $bookCommentModel->content = $this->text;
        $bookCommentModel->save();
        return true;
    }
    public function writeFacilityComment($id){
        $facilityCommentModel = new SportsFacilityComment();
        $facilityCommentModel->id_facility = $id;
        $facilityCommentModel->id_user = Yii::$app->user->identity['id'];
        $facilityCommentModel->content = $this->text;
        $facilityCommentModel->save();
        return true;
    }
}