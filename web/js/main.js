/* Operations with books */

$('.add_book').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    $.ajax({
        url: '/book/add',
        data: {id: id},
        type: 'GET',
        success: function (res) {
            alert('Прочитанные книги Вы можете посмотреть в личном кабинете');
        },
        error: function () {
            alert('Error!');
        }
    });
});

$('#modal-books .modal-body').on('click', '.delete-book', function () {
    var id = $(this).data('id');
    $.ajax({
        url: '/site/delete-book',
        data: {id: id},
        type: 'GET',
        success: function (res) {
            if (!res)
                alert('Ошибка!');
            $('#modal-books .modal-body').html(res);
            $('#modal-books').modal();
        },
        error: function() {
            alert('Error!');
        }
    });
});

function getBooks() {
    $.ajax({
        url: '/site/show-books',
        type: 'GET',
        success: function (res) {
            if(!res) alert('Ошибка!');
            $('#modal-books .modal-body').html(res);
            $('#modal-books').modal();
            // console.log(res);
        },
        error: function () {
            alert('Error!');
        }
    });
    return false;
}

$('.rate_book').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var mark = $(this).data('mark');
    $.ajax({
        url: '/book/rate',
        data: {id: id, mark: mark},
        type: 'GET',
        success: function (res) {
            alert('Книга оценена');
        },
        error: function () {
            alert('Error!');
        }
    });
});

/* End of operations with books */

/* Operations with elements */
$('.add_element').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    $.ajax({
        url: '/element/add',
        data: {id: id},
        type: 'GET',
        success: function (res) {
            alert('Изученные элементы Вы можете посмотреть в личном кабинете');
        },
        error: function () {
            alert('Error!');
        }
    });
});

function getElements() {
    $.ajax({
        url: '/site/show-elements',
        type: 'GET',
        success: function (res) {
            if(!res) alert('Ошибка!');
            $('#modal-elements .modal-body').html(res);
            $('#modal-elements').modal();
            // console.log(res);
        },
        error: function () {
            alert('Error!');
        }
    });
    return false;
}

$('#modal-elements .modal-body').on('click', '.delete-element', function () {
    var id = $(this).data('id');
    $.ajax({
        url: '/site/delete-element',
        data: {id: id},
        type: 'GET',
        success: function (res) {
            if (!res)
                alert('Ошибка!');
            $('#modal-elements .modal-body').html(res);
            $('#modal-elements').modal();
        },
        error: function() {
            alert('Error!');
        }
    });
});
/* End of operations with elements */

/* Operations with competitions */
$('.add_participant').on('click', function (e) {
    e.preventDefault();
    var id_competition = $(this).data('id_competition');
    $.ajax({
        url: '/competition/add-participant',
        data: {id_competition: id_competition},
        type: 'GET',
        success: function (res) {
            alert('Теперь Вы участник данного соревнования');
        },
        error: function () {
            alert('Error!');
        }
    });
});
/* End of operations with competitions */

/* Operations with fights */
function offerFight(id) {
    $.ajax({
        url: '/fight/creating',
        data: {id: id},
        type: 'GET',
        success: function (res) {
            if(!res) alert('Ошибка!');
            $('#modal-fight .modal-body').html(res);
            $('#modal-fight').modal();
        },
        error: function () {
            alert('Error!');
        }
    });
    return false;
}

function getFightOffers() {
    $.ajax({
        url: '/site/show-fight-offers',
        type: 'GET',
        success: function (res) {
            if(!res) alert('Ошибка!');
            $('#modal-fightOffers .modal-body').html(res);
            $('#modal-fightOffers').modal();
        },
        error: function () {
            alert('Error!');
        }
    });
    return false;
}

$('#modal-fightOffers .modal-body').on('click', '.delete-fightOffer', function () {
    var id = $(this).data('id');
    $.ajax({
        url: '/site/delete-fight-offer',
        data: {id: id},
        type: 'GET',
        success: function (res) {
            if (!res)
                alert('Ошибка!');
            $('#modal-fightOffers .modal-body').html(res);
            $('#modal-fightOffers').modal();
        },
        error: function() {
            alert('Error!');
        }
    });
});

$('#modal-fightOffers .modal-body').on('click', '.accept-fight', function () {
    var id = $(this).data('id');
    $.ajax({
        url: '/site/accept-fight',
        data: {id: id},
        type: 'GET',
        success: function (res) {
            if (!res)
                alert('Ошибка!');
            $('#modal-fightOffers .modal-body').html(res);
            $('#modal-fightOffers').modal();
        },
        error: function() {
            alert('Error!');
        }
    });
});

function getOwnFightOffers() {
    $.ajax({
        url: '/site/show-own-fight-offers',
        type: 'GET',
        success: function (res) {
            if(!res) alert('Ошибка!');
            $('#modal-fightOwnOffers .modal-body').html(res);
            $('#modal-fightOwnOffers').modal();
        },
        error: function () {
            alert('Error!');
        }
    });
    return false;
}

$('#modal-fightOwnOffers .modal-body').on('click', '.delete-ownFightOffer', function () {
    var id = $(this).data('id');
    $.ajax({
        url: '/site/delete-own-fight-offer',
        data: {id: id},
        type: 'GET',
        success: function (res) {
            if (!res)
                alert('Ошибка!');
            $('#modal-fightOwnOffers .modal-body').html(res);
            $('#modal-fightOwnOffers').modal();
        },
        error: function() {
            alert('Error!');
        }
    });
});
/* End of operations with fights */

/* Operations with challenges */
function takePartChallenge(id) {
    $.ajax({
        url: '/challenges/taking-part',
        data: {id: id},
        type: 'GET',
        success: function (res) {
            if(!res) alert('Ошибка!');
            $('#modal-challenge-taking-part .modal-body').html(res);
            $('#modal-challenge-taking-part').modal();
        },
        error: function () {
            alert('Error!');
        }
    });
    return false;
}
/* End of operations with challenges */

/* Operations with sports objects */
$('.rate_facility').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var mark = $(this).data('mark');
    $.ajax({
        url: '/map/rate-facility',
        data: {id: id, mark: mark},
        type: 'GET',
        success: function (res) {
            alert('Спортивный объект оценен');
        },
        error: function () {
            alert('Error!');
        }
    });
});
/* End of operations with sports objects */